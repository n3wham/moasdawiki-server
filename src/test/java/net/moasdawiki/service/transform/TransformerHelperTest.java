/*
 * MoasdaWiki Server
 *
 * Copyright (C) 2008 - 2023 Herbert Reiter (herbert@moasdawiki.net)
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License version 3 as
 * published by the Free Software Foundation (AGPL-3.0-only).
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

package net.moasdawiki.service.transform;

import net.moasdawiki.service.wiki.PageElementTransformer;
import net.moasdawiki.service.wiki.structure.*;
import org.jetbrains.annotations.NotNull;
import org.testng.annotations.Test;

import static org.mockito.Mockito.*;
import static org.testng.Assert.*;

public class TransformerHelperTest {

    @Test
    public void testNull() {
        WikiPage wikiPage = new WikiPage("/path", null, null, null);
        WikiPage result = TransformerHelper.transformPageElements(wikiPage, pageElement -> pageElement);
        assertNull(result.getChild());
    }

    @Test
    public void testWikiPageReplaced() {
        WikiPage wikiPage = new WikiPage("/path", null, null, null);
        WikiPage result = TransformerHelper.transformPageElements(wikiPage, pageElement -> new TextOnly("replaced"));
        assertNotNull(result);
    }

    @Test
    public void testPageElementList() {
        PageElementList pageElementList = new PageElementList();
        pageElementList.add(new TextOnly("text"));
        WikiPage wikiPage = new WikiPage("/path", pageElementList, null, null);
        //noinspection Convert2Lambda
        PageElementTransformer transformer = spy(new PageElementTransformer() {
            @Override
            public PageElement transformPageElement(@NotNull PageElement pageElement) {
                return pageElement;
            }
        });
        WikiPage result = TransformerHelper.transformPageElements(wikiPage, transformer);
        assertTrue(result.getChild() instanceof PageElementList);
        verify(transformer, times(1)).transformPageElement(argThat(pageElement -> pageElement instanceof TextOnly));
    }

    @Test
    public void testPageElementListWithNullMapping() {
        PageElementList pageElementList = new PageElementList();
        pageElementList.add(new TextOnly("text"));
        WikiPage wikiPage = new WikiPage("/path", pageElementList, null, null);
        WikiPage result = TransformerHelper.transformPageElements(wikiPage, pageElement -> {
            if (pageElement instanceof TextOnly) {
                return null;
            } else {
                return pageElement;
            }
        });
        assertTrue(result.getChild() instanceof PageElementList);
        assertEquals(((PageElementList) result.getChild()).size(), 0);
    }

    @Test
    public void testPageElementWithChild() {
        PageElementWithChild pageElementWithChild = new Bold(new TextOnly("bold text"), null, null);
        WikiPage wikiPage = new WikiPage("/path", pageElementWithChild, null, null);
        //noinspection Convert2Lambda
        PageElementTransformer transformer = spy(new PageElementTransformer() {
            @Override
            public PageElement transformPageElement(@NotNull PageElement pageElement) {
                return pageElement;
            }
        });
        TransformerHelper.transformPageElements(wikiPage, transformer);
        verify(transformer, times(1)).transformPageElement(argThat(pageElement -> pageElement instanceof TextOnly));
    }

    @Test
    public void testTable() {
        Table table = new Table(null, null, null);
        TableRow tableRow = new TableRow(null);
        tableRow.addCell(new TableCell(new TextOnly("cell content"), false, null));
        table.addRow(tableRow);
        WikiPage wikiPage = new WikiPage("/path", table, null, null);
        //noinspection Convert2Lambda
        PageElementTransformer transformer = spy(new PageElementTransformer() {
            @Override
            public PageElement transformPageElement(@NotNull PageElement pageElement) {
                return pageElement;
            }
        });
        TransformerHelper.transformPageElements(wikiPage, transformer);
        verify(transformer, times(1)).transformPageElement(argThat(pageElement -> pageElement instanceof TextOnly));
    }
}
