/*
 * MoasdaWiki Server
 *
 * Copyright (C) 2008 - 2023 Herbert Reiter (herbert@moasdawiki.net)
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License version 3 as
 * published by the Free Software Foundation (AGPL-3.0-only).
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

package net.moasdawiki.service.transform;

import net.moasdawiki.base.Logger;
import net.moasdawiki.service.repository.AnyFile;
import net.moasdawiki.service.wiki.WikiFile;
import net.moasdawiki.service.wiki.WikiService;
import net.moasdawiki.service.wiki.structure.IncludePage;
import net.moasdawiki.service.wiki.structure.PageElementList;
import net.moasdawiki.service.wiki.structure.TextOnly;
import net.moasdawiki.service.wiki.structure.WikiPage;
import org.testng.annotations.Test;

import static org.mockito.Mockito.*;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class IncludePageTransformerTest {

    @Test
    public void test() throws Exception {
        Logger logger = new Logger(null);
        WikiService wikiService = mock(WikiService.class);
        when(wikiService.getWikiFile("/include-name")).thenReturn(new WikiFile("/include-name", "content",
                new WikiPage("/include-name", new TextOnly("new-content"), null, null),
                new AnyFile("/include-name.txt")));
        IncludePageTransformer transformer = new IncludePageTransformer(logger, wikiService);
        PageElementList content = new PageElementList();
        content.add(new IncludePage("/include-name", null, null));
        WikiPage wikiPage = new WikiPage("/path", content, null, null);
        WikiPage resultWikiPage = transformer.transformWikiPage(wikiPage);
        verify(wikiService, times(1)).getWikiFile("/include-name");
        assertTrue(resultWikiPage.getChild() instanceof PageElementList);
        PageElementList resultPel = (PageElementList) resultWikiPage.getChild();
        assertTrue(resultPel.get(0) instanceof WikiPage);
        WikiPage innerWikiPage = (WikiPage) resultPel.get(0);
        assertTrue(innerWikiPage.getChild() instanceof TextOnly);
        assertEquals(((TextOnly) innerWikiPage.getChild()).getText(), "new-content");
    }
}
