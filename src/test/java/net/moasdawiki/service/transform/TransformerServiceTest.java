/*
 * MoasdaWiki Server
 *
 * Copyright (C) 2008 - 2023 Herbert Reiter (herbert@moasdawiki.net)
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License version 3 as
 * published by the Free Software Foundation (AGPL-3.0-only).
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

package net.moasdawiki.service.transform;

import net.moasdawiki.service.wiki.structure.WikiPage;
import org.testng.annotations.Test;

import static org.mockito.Mockito.*;

public class TransformerServiceTest {

    @Test
    public void test() {
        TransformWikiPage transformWikiPage = mock(TransformWikiPage.class);
        TransformerService transformerService = new TransformerService(new TransformWikiPage[]{ transformWikiPage });
        WikiPage wikiPage = new WikiPage(null, null, null, null);
        transformerService.applyTransformations(wikiPage);
        verify(transformWikiPage, times(1)).transformWikiPage(same(wikiPage));
    }
}
