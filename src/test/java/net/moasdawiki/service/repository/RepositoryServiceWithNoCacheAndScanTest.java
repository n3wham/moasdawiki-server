/*
 * MoasdaWiki Server
 *
 * Copyright (C) 2008 - 2023 Herbert Reiter (herbert@moasdawiki.net)
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License version 3 as
 * published by the Free Software Foundation (AGPL-3.0-only).
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

package net.moasdawiki.service.repository;

import net.moasdawiki.FileHelper;
import net.moasdawiki.base.Logger;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.nio.file.Path;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

/**
 * Tests for RepositoryService with missing cache file and scanRepository == true.
 */
public class RepositoryServiceWithNoCacheAndScanTest {

    private Path tempDir;
    private RepositoryService repositoryService;

    @BeforeMethod
    public void beforeMethod() throws Exception {
        tempDir = FileHelper.createTempDirectoryAndCopyFiles("src/test/resources/repository-without-cache");
        repositoryService = new RepositoryService(new Logger(null), tempDir.toFile(), null, true);
    }

    @AfterMethod
    public void afterMethod() {
        FileHelper.deleteDirectory(tempDir.toFile());
        tempDir = null;
    }

    @Test
    public void testConstructor() {
        assertEquals(repositoryService.getFiles().size(), 2);
        // Check if cache file was generated
        assertNotNull(repositoryService.getFile(RepositoryService.FILELIST_CACHE_FILEPATH));
    }
}
