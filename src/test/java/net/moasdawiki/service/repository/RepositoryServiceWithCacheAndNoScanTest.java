/*
 * MoasdaWiki Server
 *
 * Copyright (C) 2008 - 2023 Herbert Reiter (herbert@moasdawiki.net)
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License version 3 as
 * published by the Free Software Foundation (AGPL-3.0-only).
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

package net.moasdawiki.service.repository;

import net.moasdawiki.FileHelper;
import net.moasdawiki.base.Logger;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.nio.file.Path;

import static org.testng.Assert.assertTrue;

/**
 * Tests for RepositoryService with existing cache file and scanRepository == false.
 * This is used for App context.
 */
public class RepositoryServiceWithCacheAndNoScanTest {

    private Path tempDir;
    private RepositoryService repositoryService;

    @BeforeMethod
    public void beforeMethod() throws Exception {
        tempDir = FileHelper.createTempDirectoryAndCopyFiles("src/test/resources/repository-with-cache");
        repositoryService = new RepositoryService(new Logger(null), tempDir.toFile(), null, false);
    }

    @AfterMethod
    public void afterMethod() {
        FileHelper.deleteDirectory(tempDir.toFile());
        tempDir = null;
    }

    @Test
    public void testExistsFile() {
        // only looks up the cache, some of them don't exist on disk
        assertTrue(repositoryService.existsFile("/file-2019-11-01.txt"));
        assertTrue(repositoryService.existsFile("/file-2020-01-01.txt"));
        assertTrue(repositoryService.existsFile("/file-2020-01-20.txt"));
        assertTrue(repositoryService.existsFile("/file-2020-02-01.txt"));
    }
}
