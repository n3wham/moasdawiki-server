/*
 * MoasdaWiki Server
 *
 * Copyright (C) 2008 - 2023 Herbert Reiter (herbert@moasdawiki.net)
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License version 3 as
 * published by the Free Software Foundation (AGPL-3.0-only).
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

package net.moasdawiki.service.repository;

import net.moasdawiki.FileHelper;
import net.moasdawiki.base.Logger;
import net.moasdawiki.base.ServiceException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.Calendar;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;
import java.util.stream.Collectors;

import static net.moasdawiki.AssertHelper.assertContains;
import static net.moasdawiki.AssertHelper.assertContainsNot;
import static org.testng.Assert.*;

/**
 * Tests for RepositoryService with existing cache file and scanRepository == true.
 */
public class RepositoryServiceWithCacheAndScanTest {

    private Path tempDir;
    private RepositoryService repositoryService;

    @BeforeMethod
    public void beforeMethod() throws Exception {
        tempDir = FileHelper.createTempDirectoryAndCopyFiles("src/test/resources/repository-with-cache");
        repositoryService = new RepositoryService(new Logger(null), tempDir.toFile(), null, true);
    }

    @AfterMethod
    public void afterMethod() {
        FileHelper.deleteDirectory(tempDir.toFile());
        tempDir = null;
    }

    @Test
    public void testExistsFile() {
        {
            // file initially in cache but not on disk -> clean up cache
            String filePath = "/file-2019-11-01.txt";
            assertNotNull(repositoryService.getFile(filePath));
            assertFalse(repositoryService.existsFile(filePath));
            assertNull(repositoryService.getFile(filePath));
        }
        {
            // existing file
            String filePath = "/file-2020-01-01.txt";
            assertNotNull(repositoryService.getFile(filePath));
            assertTrue(repositoryService.existsFile(filePath));
        }
    }

    @Test
    public void testGetFile() {
        assertNotNull(repositoryService.getFile("/file-2020-01-01.txt"));
        assertNull(repositoryService.getFile("/not-existing.txt"));
    }

    @Test
    public void testCacheTimestamp() {
        // 2020-01-20T21:39:58.804Z
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        cal.set(2020, Calendar.JANUARY, 20, 21, 39, 58);
        cal.set(Calendar.MILLISECOND, 804);
        AnyFile file = repositoryService.getFile("/file-2020-01-20.txt");
        assertNotNull(file);
        assertEquals(file.getContentTimestamp().getTime(), cal.getTimeInMillis());
    }

    @Test
    public void testGetFiles() {
        // Number of entries in the cache file
        assertEquals(repositoryService.getFiles().size(), 5);
    }

    @Test
    public void testGetModifiedAfterDate() {
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        cal.clear();
        cal.set(2020, Calendar.JANUARY, 1);
        Set<AnyFile> files = repositoryService.getModifiedAfter(cal.getTime());
        Set<String> filePaths = files.stream().map(AnyFile::getFilePath).collect(Collectors.toSet());
        assertContainsNot(filePaths, "/file-2019-11-01.txt");
        assertContainsNot(filePaths, "/file-2020-01-01.txt");
        assertContains(filePaths, "/file-2020-01-20.txt");
        assertContains(filePaths, "/file-2020-02-01.txt");
    }

    @Test
    public void testGetModifiedAfterNull() {
        Set<AnyFile> files = repositoryService.getModifiedAfter(null);
        assertEquals(files.size(), 5);
    }

    @Test
    public void testGetLastModifiedFilesWithFilter() {
        List<AnyFile> files = repositoryService.getLastModifiedFiles(1, anyFile -> anyFile.getFilePath().contains("2019"));
        assertEquals(files.size(), 1);
        assertEquals(files.get(0).getFilePath(), "/file-2019-11-01.txt");
    }

    @Test
    public void testGetLastModifiedFilesNoFilter() {
        List<AnyFile> files = repositoryService.getLastModifiedFiles(1, anyFile -> true);
        assertEquals(files.size(), 1);
        assertTrue(files.get(0).getFilePath().equals("/file-2020-02-01.txt") ||
                files.get(0).getFilePath().equals("/filelist.cache"));
    }

    @Test
    public void testDeleteFile() throws Exception {
        AnyFile anyFile = new AnyFile("/tmp-file.txt");
        repositoryService.writeBinaryFile(anyFile, new byte[0], null);
        assertTrue(repositoryService.existsFile(anyFile.getFilePath()));
        repositoryService.deleteFile(new AnyFile("/tmp-file.txt"));
        assertFalse(repositoryService.existsFile(anyFile.getFilePath()));
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testDeleteFileUnknown() throws Exception {
        repositoryService.deleteFile(new AnyFile("/not-existing.txt"));
    }

    @Test
    public void testReadTextFile() throws Exception {
        String content = repositoryService.readTextFile(new AnyFile("/file-2020-01-01.txt"));
        assertEquals(content, "content in repository-with-cache");
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testReadTextFileNotExisting() throws Exception {
        repositoryService.readTextFile(new AnyFile("/not-existing.txt"));
    }

    @Test
    public void testWriteTextFile() throws Exception {
        // Write file
        AnyFile anyFile = repositoryService.writeTextFile(new AnyFile("/tmp-file.txt"), "testcontent");
        assertNotNull(anyFile);
        assertEquals(anyFile.getFilePath(), "/tmp-file.txt");

        // Check written content
        String contentRead = FileHelper.readTextFile(tempDir.toString() + "/tmp-file.txt");
        assertEquals(contentRead, "testcontent");
    }

    @Test
    public void testReadBinaryFile() throws Exception {
        byte[] contentBytes = repositoryService.readBinaryFile(new AnyFile("/file-2020-01-01.txt"));
        assertEquals(contentBytes, "content in repository-with-cache".getBytes(StandardCharsets.UTF_8));
    }

    @Test
    public void testReadBinaryFileNotInCache() throws Exception {
        // Before readBinaryFile()
        assertNull(repositoryService.getFile("/new-file.bin"));

        // Create and read new file
        try (Writer writer = new FileWriter(tempDir.toString() + "/new-file.bin")) {
            writer.append("testcontent");
        }
        byte[] contentBytes = repositoryService.readBinaryFile(new AnyFile("/new-file.bin"));
        assertEquals(contentBytes, "testcontent".getBytes(StandardCharsets.UTF_8));

        // After readBinaryFile()
        assertNotNull(repositoryService.getFile("/new-file.bin"));
    }

    @Test(expectedExceptions = ServiceException.class)
    public void testReadBinaryFileNotExisting() throws Exception {
        repositoryService.readBinaryFile(new AnyFile("/not-existing.txt"));
    }

    @Test
    public void testWriteBinaryFile() throws Exception {
        // Write file
        byte[] content = "testcontent".getBytes(StandardCharsets.UTF_8);
        AnyFile anyFile = repositoryService.writeBinaryFile(new AnyFile("/tmp-file.bin"), content, null);
        assertNotNull(anyFile);
        assertEquals(anyFile.getFilePath(), "/tmp-file.bin");

        // Check written content
        String contentRead = FileHelper.readTextFile(tempDir.toString() + "/tmp-file.bin");
        assertEquals(contentRead, "testcontent");
    }

    @Test
    public void testCreateFolders() throws Exception {
        File file = new File(tempDir.toFile(), "folder1/subfolder1/file.txt");
        repositoryService.createFolders(file);
        assertTrue(new File(tempDir.toFile(), "folder1/subfolder1").exists());
    }

    @Test
    public void testRepository2FilesystemPath() {
        //noinspection ConstantConditions
        assertNull(repositoryService.repository2FilesystemPath(null, false));
        assertEquals(repositoryService.repository2FilesystemPath("", false), tempDir.toString() + File.separator);
        assertEquals(repositoryService.repository2FilesystemPath("/", false), tempDir.toString() + File.separator);
        assertEquals(repositoryService.repository2FilesystemPath("/a", false), new File(tempDir.toString(), "a").getAbsolutePath());
        assertEquals(repositoryService.repository2FilesystemPath("/a/b", false), new File(tempDir.toString(), "a/b").getAbsolutePath());
        assertEquals(repositoryService.repository2FilesystemPath("/forbidden\"%*:<>?\\|characters", false), new File(tempDir.toString(), "forbidden%0022%0025%002a%003a%003c%003e%003f%005c%007ccharacters").getAbsolutePath());
        assertEquals(repositoryService.repository2FilesystemPath("/a/./b", false), new File(tempDir.toString(), "a/%002e/b").getAbsolutePath());
        assertEquals(repositoryService.repository2FilesystemPath("/a/../b", false), new File(tempDir.toString(), "a/%002e%002e/b").getAbsolutePath());
    }

    @Test
    public void testFilesystem2RepositoryPath() {
        //noinspection ConstantConditions
        assertNull(repositoryService.filesystem2RepositoryPath(null));
        assertNull(repositoryService.filesystem2RepositoryPath("/outside-repository"));
        assertEquals(repositoryService.filesystem2RepositoryPath(tempDir.toString()), "/");
        assertEquals(repositoryService.filesystem2RepositoryPath(new File(tempDir.toString(), "a").getAbsolutePath()), "/a");
        assertEquals(repositoryService.filesystem2RepositoryPath(new File(tempDir.toString(), "a/b").getAbsolutePath()), "/a/b");
        assertEquals(repositoryService.filesystem2RepositoryPath(new File(tempDir.toString(), "forbidden%0022%0025%002a%003a%003c%003e%003f%005c%007ccharacters").getAbsolutePath()), "/forbidden\"%*:<>?\\|characters");
        assertNull(repositoryService.filesystem2RepositoryPath(new File(tempDir.toString(), "invalid%klmncharacters").getAbsolutePath()));
    }
}
