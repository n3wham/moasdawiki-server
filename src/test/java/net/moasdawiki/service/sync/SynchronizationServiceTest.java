/*
 * MoasdaWiki Server
 *
 * Copyright (C) 2008 - 2023 Herbert Reiter (herbert@moasdawiki.net)
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License version 3 as
 * published by the Free Software Foundation (AGPL-3.0-only).
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

package net.moasdawiki.service.sync;

import net.moasdawiki.AssertHelper;
import net.moasdawiki.base.Logger;
import net.moasdawiki.base.Settings;
import net.moasdawiki.http.HttpRequest;
import net.moasdawiki.service.HttpResponse;
import net.moasdawiki.service.repository.AnyFile;
import net.moasdawiki.service.repository.RepositoryService;
import net.moasdawiki.util.DateUtils;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.testng.Assert.*;

public class SynchronizationServiceTest {

    private Logger logger;
    private Settings settings;
    private RepositoryService repositoryService;
    private SynchronizationService synchronizationService;

    @BeforeMethod
    public void beforeMethod() throws Exception {
        logger = new Logger(null);
        settings = mock(Settings.class);
        when(settings.getProgramName()).thenReturn("MoasdaWiki");
        when(settings.getVersion()).thenReturn("version");
        when(settings.getServerHost()).thenReturn("host");
        repositoryService = mock(RepositoryService.class);
        AnyFile anyFile = new AnyFile("/path");
        when(repositoryService.getFile(anyString())).thenReturn(anyFile);
        String fileContent = "serverSessionId\tclientSessionId\t2021-01-01T12:00:00.000Z\tclientName\tclientVersion\tclientHost\tfalse\n";
        when(repositoryService.readTextFile(same(anyFile))).thenReturn(fileContent);
        synchronizationService = new SynchronizationService(logger, settings, repositoryService);
    }

    @Test
    public void testReadSessionList_Exists() throws Exception {
        List<SessionData> sessions = synchronizationService.getSessions();
        assertEquals(sessions.size(), 1);
        SessionData sessionData = sessions.get(0);
        assertEquals(sessionData.serverSessionId, "serverSessionId");
        assertEquals(sessionData.clientSessionId, "clientSessionId");
        assertEquals(sessionData.createTimestamp, DateUtils.parseUtcDate("2021-01-01T12:00:00.000Z"));
        assertEquals(sessionData.clientName, "clientName");
        assertEquals(sessionData.clientVersion, "clientVersion");
        assertEquals(sessionData.clientHost, "clientHost");
        assertFalse(sessionData.authorized);
        assertNull(sessionData.lastSyncTimestamp);
    }

    @Test
    public void testReadSessionList_NotExists() {
        reset(repositoryService);
        SynchronizationService synchronizationService2 = new SynchronizationService(logger, settings, repositoryService);
        List<SessionData> sessions = synchronizationService2.getSessions();
        AssertHelper.assertIsEmpty(sessions);
    }

    @Test
    public void testSessionPermit() {
        HttpRequest httpRequest = createHttpRequestGet("/sync-gui/session-permit", "serverSessionId");
        assertFalse(synchronizationService.getSession("serverSessionId").authorized);
        synchronizationService.handleSyncRequest(httpRequest);
        assertTrue(synchronizationService.getSession("serverSessionId").authorized);
    }

    @Test
    public void testSessionPermit_NoSessionId() {
        HttpRequest httpRequest = createHttpRequestGet("/sync-gui/session-permit", null);
        synchronizationService.handleSyncRequest(httpRequest);
        assertFalse(synchronizationService.getSession("serverSessionId").authorized);
    }

    @Test
    public void testSessionPermit_InvalidSessionId() {
        HttpRequest httpRequest = createHttpRequestGet("/sync-gui/session-permit", "invalid");
        synchronizationService.handleSyncRequest(httpRequest);
        assertFalse(synchronizationService.getSession("serverSessionId").authorized);
    }

    @Test
    public void testSessionDrop() {
        HttpRequest httpRequest = createHttpRequestGet("/sync-gui/session-drop", "serverSessionId");
        assertNotNull(synchronizationService.getSession("serverSessionId"));
        synchronizationService.handleSyncRequest(httpRequest);
        assertNull(synchronizationService.getSession("serverSessionId"));
    }

    @Test
    public void testCreateSession() {
        String bodyText = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<create-session version=\"2.0\">\n" +
                "  <client-session-id>abcdefghi</client-session-id>\n" +
                "  <client-name>MoasdaWiki App</client-name>\n" +
                "  <client-version>2.1</client-version>\n" +
                "  <client-host>ANDROIDPHONE1</client-host>\n" +
                "</create-session>";
        HttpRequest httpRequest = createHttpRequestPost("/sync/create-session", bodyText);
        HttpResponse httpResponse = synchronizationService.handleSyncRequest(httpRequest);
        assertNotNull(httpResponse);
        String responseXml = new String(httpResponse.getContent(), StandardCharsets.UTF_8);
        AssertHelper.assertContains(responseXml, "<server-session-id>");
        AssertHelper.assertContains(responseXml, "<server-name>");
        AssertHelper.assertContains(responseXml, "<server-version>");
        AssertHelper.assertContains(responseXml, "<server-host>");
        String sessionId = extractServerSessionId(responseXml);
        assertNotNull(synchronizationService.getSession(sessionId));
    }

    @Test
    public void testCheckSession() {
        SessionData sessionData = synchronizationService.getSessions().get(0);
        String bodyText = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<check-session version=\"2.0\">\n" +
                "  <server-session-id>" + sessionData.serverSessionId + "</server-session-id>\n" +
                "</check-session>";
        HttpRequest httpRequest = createHttpRequestPost("/sync/check-session", bodyText);
        HttpResponse httpResponse = synchronizationService.handleSyncRequest(httpRequest);
        assertNotNull(httpResponse);
        String responseXml = new String(httpResponse.getContent(), StandardCharsets.UTF_8);
        AssertHelper.assertContains(responseXml, "<valid>true</valid>");
        AssertHelper.assertContains(responseXml, "<authorized>false</authorized>");
        AssertHelper.assertContains(responseXml, "<client-session-id>" + sessionData.clientSessionId + "</client-session-id>");
    }

    @Test
    public void testCheckSession_NoSession() {
        String bodyText = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<check-session version=\"2.0\">\n" +
                "</check-session>";
        HttpRequest httpRequest = createHttpRequestPost("/sync/check-session", bodyText);
        HttpResponse httpResponse = synchronizationService.handleSyncRequest(httpRequest);
        assertNotNull(httpResponse);
        String responseXml = new String(httpResponse.getContent(), StandardCharsets.UTF_8);
        AssertHelper.assertContains(responseXml, "<error");
        AssertHelper.assertContains(responseXml, "<message>");
    }

    @Test
    public void testListModifiedFiles() throws Exception {
        when(repositoryService.getModifiedAfter(any())).thenReturn(Collections.singleton(
                new AnyFile("/page-name", DateUtils.parseUtcDate("2021-02-01T12:00:00.000Z"))));
        SessionData sessionData = synchronizationService.getSessions().get(0);
        sessionData.authorized = true;
        String bodyText = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<list-modified-files version=\"2.0\">\n" +
                "  <server-session-id>" + sessionData.serverSessionId + "</server-session-id>\n" +
                "  <last-sync-server-time>2021-01-01T12:00:00.000Z</last-sync-server-time>\n" +
                "</list-modified-files>";
        HttpRequest httpRequest = createHttpRequestPost("/sync/list-modified-files", bodyText);
        HttpResponse httpResponse = synchronizationService.handleSyncRequest(httpRequest);
        assertNotNull(httpResponse);
        String responseXml = new String(httpResponse.getContent(), StandardCharsets.UTF_8);
        AssertHelper.assertContains(responseXml, "current-server-time");
        AssertHelper.assertContains(responseXml, "<file timestamp=\"2021-02-01T12:00:00.000Z\">/page-name</file>");
        verify(repositoryService).getModifiedAfter(argThat(modifiedAfter -> DateUtils.formatUtcDate(modifiedAfter).equals("2021-01-01T12:00:00.000Z")));
    }

    @Test
    public void testReadFile() throws Exception {
        AnyFile anyFile = new AnyFile("/page-name.txt", DateUtils.parseUtcDate("2021-02-01T12:00:00.000Z"));
        when(repositoryService.getFile(anyString())).thenReturn(anyFile);
        when(repositoryService.readBinaryFile(any())).thenReturn("content".getBytes(StandardCharsets.UTF_8));
        SessionData sessionData = synchronizationService.getSessions().get(0);
        sessionData.authorized = true;
        String bodyText = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<read-file version=\"2.0\">\n" +
                "  <server-session-id>" + sessionData.serverSessionId + "</server-session-id>\n" +
                "  <file-path>/page-name.txt</file-path>\n" +
                "</read-file>";
        HttpRequest httpRequest = createHttpRequestPost("/sync/read-file", bodyText);
        HttpResponse httpResponse = synchronizationService.handleSyncRequest(httpRequest);
        assertNotNull(httpResponse);
        String responseXml = new String(httpResponse.getContent(), StandardCharsets.UTF_8);
        AssertHelper.assertContains(responseXml, "<timestamp>2021-02-01T12:00:00.000Z</timestamp>");
        AssertHelper.assertContains(responseXml, "<content>Y29udGVudA==</content>");
        verify(repositoryService).getFile(eq("/page-name.txt"));
        verify(repositoryService).readBinaryFile(same(anyFile));
    }

    private HttpRequest createHttpRequestGet(String urlPath, String sessionId) {
        return new HttpRequest(Collections.emptyMap(), HttpRequest.METHOD_GET, urlPath, urlPath,
                Collections.singletonMap("session-id", sessionId), new byte[0]);
    }

    private HttpRequest createHttpRequestPost(String urlPath, String bodyText) {
        return new HttpRequest(Collections.emptyMap(), HttpRequest.METHOD_POST, urlPath, urlPath,
                Collections.emptyMap(), bodyText.getBytes(StandardCharsets.UTF_8));
    }

    private String extractServerSessionId(String xml) {
        int pos1 = xml.indexOf("<server-session-id>") + "<server-session-id>".length();
        int pos2 = xml.indexOf("</server-session-id>");
        return xml.substring(pos1, pos2);
    }
}
