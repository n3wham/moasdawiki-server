/*
 * MoasdaWiki Server
 *
 * Copyright (C) 2008 - 2023 Herbert Reiter (herbert@moasdawiki.net)
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License version 3 as
 * published by the Free Software Foundation (AGPL-3.0-only).
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

package net.moasdawiki.service.search;

import net.moasdawiki.base.Logger;
import net.moasdawiki.service.repository.AnyFile;
import net.moasdawiki.service.wiki.WikiFile;
import net.moasdawiki.service.wiki.WikiService;
import net.moasdawiki.service.wiki.structure.WikiPage;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import static net.moasdawiki.AssertHelper.assertContains;
import static net.moasdawiki.AssertHelper.assertIsEmpty;
import static org.mockito.Mockito.*;
import static org.testng.Assert.*;

public class SearchServiceTest {

    private Logger logger;
    private WikiService wikiService;
    private SearchIgnoreList searchIgnoreList;
    private SearchIndex searchIndex;
    private SearchService searchService;

    @BeforeMethod
    public void beforeMethod() {
        logger = new Logger(null);
        wikiService = mock(WikiService.class);
        searchIgnoreList = mock(SearchIgnoreList.class);
        searchIndex = mock(SearchIndex.class);
        searchService = new SearchService(logger, wikiService, searchIgnoreList, searchIndex, false);
    }

    @Test
    public void testReset() {
        searchService.reset();
        verify(searchIgnoreList, times(1)).reset();
        verify(searchIndex, times(1)).reset();
    }

    @Test
    public void testParseQueryStringSingleWord() {
        Set<String> words = searchService.parseQueryString("word");
        assertEquals(words.size(), 1);
        assertContains(words, "word");
    }

    @Test
    public void testParseQueryStringSpecialCharacters() {
        // ignore special characters
        Set<String> words = searchService.parseQueryString(",.(\"word\") -=");
        assertEquals(words.size(), 1);
        assertContains(words, "word");
    }

    @Test
    public void testParseQueryStringMultipleWords() {
        Set<String> words = searchService.parseQueryString("word1 word2");
        assertEquals(words.size(), 2);
        assertContains(words, "word1");
        assertContains(words, "word2");
    }

    @Test
    public void testParseQueryStringEmpty() {
        Set<String> words = searchService.parseQueryString("");
        assertIsEmpty(words);
    }

    @Test
    public void testSearchInRepositoryEmpty() throws Exception {
        assertIsEmpty(searchService.searchInRepository(Collections.emptySet()));
    }

    @Test
    public void testSearchInRepositoryNoMatch() throws Exception {
        Set<String> words = Collections.singleton("word");
        searchService.searchInRepository(words);
        verify(searchIndex, times(1)).searchWikiFilePaths(same(words));
    }

    @Test
    public void testSearchInRepositoryWithMatch() throws Exception {
        Set<String> wikiFilePaths = new HashSet<>();
        wikiFilePaths.add("/path1");
        wikiFilePaths.add("/path2");
        when(searchIndex.searchWikiFilePaths(any())).thenReturn(wikiFilePaths);
        Set<String> words = Collections.singleton("word");
        searchService.searchInRepository(words);
        verify(searchIndex, times(1)).searchWikiFilePaths(same(words));
        verify(wikiService, never()).getWikiFile(anyString());
    }

    @Test
    public void testSearchInRepositoryWithMatchAndRepositoryScan() throws Exception {
        searchService = new SearchService(logger, wikiService, searchIgnoreList, searchIndex, true);
        when(searchIndex.searchWikiFilePaths(any())).thenReturn(Collections.singleton("/path"));
        when(wikiService.getWikiFile(anyString())).thenReturn(new WikiFile("/path", "content",
                new WikiPage(null, null, null, null), new AnyFile("/path.txt")));
        Set<String> words = Collections.singleton("word");
        searchService.searchInRepository(words);
        verify(searchIndex, times(1)).searchWikiFilePaths(same(words));
        verify(wikiService, times(1)).getWikiFile(anyString());
    }

    @Test
    public void testScanPageTitlePathMatch() {
        SearchService.MatchingCategories mc = new SearchService.MatchingCategories();
        PageDetails.MatchingLine matchingLine = searchService.scanPageTitle("/path", Pattern.compile("/path"), mc);
        assertTrue(mc.titleComplete);
        assertEquals(mc.titleWord, 1);
        assertEquals(matchingLine.getPositions().size(), 1);
        assertEquals(matchingLine.getPositions().get(0).getFrom(), 0);
        assertEquals(matchingLine.getPositions().get(0).getTo(), 5);
    }

    @Test
    public void testScanPageTitleNameMatch() {
        SearchService.MatchingCategories mc = new SearchService.MatchingCategories();
        PageDetails.MatchingLine matchingLine = searchService.scanPageTitle("/path", Pattern.compile("path"), mc);
        assertTrue(mc.titleComplete);
        assertEquals(mc.titleWord, 1);
        assertEquals(matchingLine.getPositions().size(), 1);
        assertEquals(matchingLine.getPositions().get(0).getFrom(), 1); // without "/"
        assertEquals(matchingLine.getPositions().get(0).getTo(), 5);
    }

    @Test
    public void testScanPageTitleSubstringWordMatch() {
        SearchService.MatchingCategories mc = new SearchService.MatchingCategories();
        PageDetails.MatchingLine matchingLine = searchService.scanPageTitle("/My Page Name", Pattern.compile("Page"), mc);
        assertFalse(mc.titleComplete);
        assertEquals(mc.titleWord, 1);
        assertEquals(mc.titleSubstring, 0);
        assertEquals(matchingLine.getPositions().size(), 1);
        assertEquals(matchingLine.getPositions().get(0).getFrom(), 4);
        assertEquals(matchingLine.getPositions().get(0).getTo(), 8);
    }

    @Test
    public void testScanPageTitleSubstringMatch() {
        SearchService.MatchingCategories mc = new SearchService.MatchingCategories();
        PageDetails.MatchingLine matchingLine = searchService.scanPageTitle("/My Page Name", Pattern.compile("am"), mc);
        assertFalse(mc.titleComplete);
        assertEquals(mc.titleWord, 0);
        assertEquals(mc.titleSubstring, 1);
        assertEquals(matchingLine.getPositions().size(), 1);
        assertEquals(matchingLine.getPositions().get(0).getFrom(), 10);
        assertEquals(matchingLine.getPositions().get(0).getTo(), 12);
    }

    @Test
    public void testScanPageTextHeadingWord() throws Exception {
        String wikiText = "= heading";
        SearchService.MatchingCategories mc = new SearchService.MatchingCategories();
        List<PageDetails.MatchingLine> matchingLines = searchService.scanPageText("", wikiText, Pattern.compile("heading"), mc);
        assertEquals(mc.headingWord, 1);
        assertEquals(mc.headingSubstring, 0);
        assertEquals(mc.paragraphWord, 0);
        assertEquals(mc.paragraphSubstring, 0);
        assertEquals(matchingLines.size(), 1);
        assertEquals(matchingLines.get(0).getPositions().size(), 1);
        assertEquals(matchingLines.get(0).getPositions().get(0).getFrom(), 2);
        assertEquals(matchingLines.get(0).getPositions().get(0).getTo(), 9);
    }

    @Test
    public void testScanPageTextHeadingSubstring() throws Exception {
        String wikiText = "= heading";
        SearchService.MatchingCategories mc = new SearchService.MatchingCategories();
        List<PageDetails.MatchingLine> matchingLines = searchService.scanPageText("", wikiText, Pattern.compile("head"), mc);
        assertEquals(mc.headingWord, 0);
        assertEquals(mc.headingSubstring, 1);
        assertEquals(mc.paragraphWord, 0);
        assertEquals(mc.paragraphSubstring, 0);
        assertEquals(matchingLines.size(), 1);
        assertEquals(matchingLines.get(0).getPositions().size(), 1);
        assertEquals(matchingLines.get(0).getPositions().get(0).getFrom(), 2);
        assertEquals(matchingLines.get(0).getPositions().get(0).getTo(), 6);
    }

    @Test
    public void testScanPageTextParagraphWord() throws Exception {
        String wikiText = "some words in paragraph";
        SearchService.MatchingCategories mc = new SearchService.MatchingCategories();
        List<PageDetails.MatchingLine> matchingLines = searchService.scanPageText("", wikiText, Pattern.compile("words"), mc);
        assertEquals(mc.headingWord, 0);
        assertEquals(mc.headingSubstring, 0);
        assertEquals(mc.paragraphWord, 1);
        assertEquals(mc.paragraphSubstring, 0);
        assertEquals(matchingLines.size(), 1);
        assertEquals(matchingLines.get(0).getPositions().size(), 1);
        assertEquals(matchingLines.get(0).getPositions().get(0).getFrom(), 5);
        assertEquals(matchingLines.get(0).getPositions().get(0).getTo(), 10);
    }

    @Test
    public void testScanPageTextParagraphSubstring() throws Exception {
        String wikiText = "some words in paragraph";
        SearchService.MatchingCategories mc = new SearchService.MatchingCategories();
        List<PageDetails.MatchingLine> matchingLines = searchService.scanPageText("", wikiText, Pattern.compile("wor"), mc);
        assertEquals(mc.headingWord, 0);
        assertEquals(mc.headingSubstring, 0);
        assertEquals(mc.paragraphWord, 0);
        assertEquals(mc.paragraphSubstring, 1);
        assertEquals(matchingLines.size(), 1);
        assertEquals(matchingLines.get(0).getPositions().size(), 1);
        assertEquals(matchingLines.get(0).getPositions().get(0).getFrom(), 5);
        assertEquals(matchingLines.get(0).getPositions().get(0).getTo(), 8);
    }

    @Test
    public void testScanPageTextUnicode() throws Exception {
        String wikiText = "Registered® Trademark™ ligatureﬁﬆÆŒ umlauteäöüßé unicode";
        SearchService.MatchingCategories mc = new SearchService.MatchingCategories();
        List<PageDetails.MatchingLine> matchingLines = searchService.scanPageText("", wikiText, Pattern.compile("uni"), mc);
        assertEquals(matchingLines.size(), 1);
        assertEquals(matchingLines.get(0).getPositions().size(), 1);
        assertEquals(matchingLines.get(0).getPositions().get(0).getFrom(), 49);
        assertEquals(matchingLines.get(0).getPositions().get(0).getTo(), 52);
    }

    @Test
    public void testExpandUmlaute() {
        assertEquals(searchService.expandUmlaute("content"), "content");
        assertEquals(searchService.expandUmlaute("textäöüßwithaeoeuessumlaute"), "text(ä|ae)(ö|oe)(ü|ue)(ß|ss)with(ä|ae)(ö|oe)(ü|ue)(ß|ss)umlaute");
    }
}
