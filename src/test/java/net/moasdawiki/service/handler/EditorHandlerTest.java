/*
 * MoasdaWiki Server
 *
 * Copyright (C) 2008 - 2023 Herbert Reiter (herbert@moasdawiki.net)
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License version 3 as
 * published by the Free Software Foundation (AGPL-3.0-only).
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

package net.moasdawiki.service.handler;

import net.moasdawiki.base.Logger;
import net.moasdawiki.base.Messages;
import net.moasdawiki.base.ServiceException;
import net.moasdawiki.base.Settings;
import net.moasdawiki.http.HttpRequest;
import net.moasdawiki.http.StatusCode;
import net.moasdawiki.service.HttpResponse;
import net.moasdawiki.service.render.HtmlService;
import net.moasdawiki.service.repository.AnyFile;
import net.moasdawiki.service.repository.RepositoryService;
import net.moasdawiki.service.wiki.WikiFile;
import net.moasdawiki.service.wiki.WikiService;
import net.moasdawiki.service.wiki.WikiText;
import net.moasdawiki.service.wiki.structure.WikiPage;
import org.jetbrains.annotations.NotNull;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.*;
import static org.testng.Assert.*;

public class EditorHandlerTest {

    private Settings settings;
    private RepositoryService repositoryService;
    private WikiService wikiService;
    private HtmlService htmlService;
    private EditorHandler editorHandler;

    @BeforeMethod
    public void beforeMethod() {
        Logger logger = new Logger(null);
        settings = mock(Settings.class);
        when(settings.getStartpagePath()).thenReturn("/startpage");
        Messages messages = mock(Messages.class);
        repositoryService = mock(RepositoryService.class);
        wikiService = mock(WikiService.class);
        htmlService = mock(HtmlService.class);
        editorHandler = new EditorHandler(logger, settings, messages, repositoryService, wikiService, htmlService);
    }

    @Test
    public void testGenerateEditorContentJsonNewPage() throws Exception {
        when(settings.getTemplatesPagePath()).thenReturn("/template");
        WikiFile templateWikiFile = new WikiFile("/template", "wiki-template", new WikiPage("/template", null, null, null), new AnyFile("/template.txt"));
        templateWikiFile.getChildren().add("/my-template");
        when(wikiService.getWikiFile(eq("/template"))).thenReturn(templateWikiFile);
        WikiFile template2WikiFile = new WikiFile("/my-template", "{{parent:/template}}\r\nmy-template-content", new WikiPage("/my-template", null, null, null), new AnyFile("/my-template.txt"));
        when(wikiService.getWikiFile(eq("/my-template"))).thenReturn(template2WikiFile);
        HttpRequest httpRequest = buildHttpRequest("/edit/new-page", "json", "");
        HttpResponse httpResponse = editorHandler.handleEditRequest(httpRequest);
        String resultJson = new String(httpResponse.getContent(), StandardCharsets.UTF_8);
        assertTrue(resultJson.contains("\"page-path\": \"/new-page\""));
        assertTrue(resultJson.contains("\"template-names\": [\"/my-template\"]"));
        assertTrue(resultJson.contains("\"template-text--/my-template\": \"my-template-content\""));
        verify(wikiService, times(1)).getWikiFile(eq("/template"));
        verify(wikiService, times(1)).getWikiFile(eq("/my-template"));
    }

    @Test
    public void testGenerateEditorContentJsonExistingPage() throws Exception {
        when(wikiService.existsWikiFile(eq("/existing-page"))).thenReturn(true);
        when(wikiService.readWikiText(eq("/existing-page"), any(), any())).thenReturn(new WikiText("content"));
        HttpRequest httpRequest = buildHttpRequest("/edit/existing-page", "json", "");
        editorHandler.handleEditRequest(httpRequest);
        verify(wikiService, times(1)).readWikiText(eq("/existing-page"), isNull(), isNull());
    }

    @Test
    public void testGenerateEditorContentJsonExistingPageSection() throws Exception {
        when(wikiService.existsWikiFile(eq("/existing-page"))).thenReturn(true);
        when(wikiService.readWikiText(eq("/existing-page"), any(), any())).thenReturn(new WikiText("content"));
        HttpRequest httpRequest = buildHttpRequest("/edit/existing-page", "json", "", "fromPos", "4", "toPos", "7");
        editorHandler.handleEditRequest(httpRequest);
        verify(wikiService, times(1)).readWikiText(eq("/existing-page"), eq(4), eq(7));
    }

    @Test
    public void testCancelOnExistingPage() {
        HttpRequest httpRequest = buildHttpRequest("/edit/abc", "cancel", "");
        editorHandler.handleEditRequest(httpRequest);
        verify(htmlService, times(1)).generateRedirectToWikiPage(eq("/abc"));
    }

    @Test
    public void testCancelOnNewPage() {
        HttpRequest httpRequest = buildHttpRequest("/edit/", "cancel", "");
        editorHandler.handleEditRequest(httpRequest);
        verify(htmlService, times(1)).generateRedirectToWikiPage(eq("/"));
    }

    @Test
    public void testDelete() throws Exception {
        HttpRequest httpRequest = buildHttpRequest("/edit/abc", "delete", "");
        editorHandler.handleEditRequest(httpRequest);
        verify(wikiService, times(1)).deleteWikiFile(eq("/abc"));
        verify(htmlService, times(1)).generateRedirectToWikiPage(eq("/startpage"));
    }

    @Test
    public void testSaveNewPage() throws Exception {
        Map<String, String> parameters = new HashMap<>();
        parameters.put("save", "");
        parameters.put("titleeditor", "/new-name");
        parameters.put("contenteditor", "content");
        HttpRequest httpRequest = new HttpRequest(Collections.emptyMap(), HttpRequest.METHOD_GET, "", "/edit/", parameters, new byte[0]);
        editorHandler.handleEditRequest(httpRequest);
        verify(wikiService, times(1)).writeWikiText(eq("/new-name"),
                argThat(wikiText -> wikiText.getText().equals("content") && wikiText.getFromPos() == null && wikiText.getToPos() == null));
        verify(htmlService, times(1)).generateRedirectToWikiPage(eq("/new-name"), isNull());
    }

    @Test
    public void testSaveModifiedPage() throws Exception {
        Map<String, String> parameters = new HashMap<>();
        parameters.put("save", "");
        parameters.put("titleeditor", "/page-name");
        parameters.put("contenteditor", "content");
        parameters.put("fromPos", "1");
        parameters.put("toPos", "2");
        HttpRequest httpRequest = new HttpRequest(Collections.emptyMap(), HttpRequest.METHOD_GET, "", "/edit/page-name", parameters, new byte[0]);
        editorHandler.handleEditRequest(httpRequest);
        //noinspection ConstantConditions
        verify(wikiService, times(1)).writeWikiText(eq("/page-name"),
                argThat(wikiText -> wikiText.getText().equals("content") && wikiText.getFromPos() == 1 && wikiText.getToPos() == 2));
        verify(htmlService, times(1)).generateRedirectToWikiPage(eq("/page-name"), isNull());
    }

    @Test
    public void testSaveModifiedPageSection() {
        Map<String, String> parameters = new HashMap<>();
        parameters.put("save", "");
        parameters.put("titleeditor", "/page-name");
        parameters.put("contenteditor", "= heading =\nother text");
        parameters.put("fromPos", "1");
        parameters.put("toPos", "2");
        HttpRequest httpRequest = new HttpRequest(Collections.emptyMap(), HttpRequest.METHOD_GET, "", "/edit/page-name", parameters, new byte[0]);
        editorHandler.handleEditRequest(httpRequest);
        verify(htmlService, times(1)).generateRedirectToWikiPage(eq("/page-name"), eq("heading"));
    }

    @Test
    public void testSaveRenamePage() throws Exception {
        Map<String, String> parameters = new HashMap<>();
        parameters.put("save", "");
        parameters.put("titleeditor", "/new-name");
        parameters.put("contenteditor", "content");
        HttpRequest httpRequest = new HttpRequest(Collections.emptyMap(), HttpRequest.METHOD_GET, "", "/edit/old-name", parameters, new byte[0]);
        editorHandler.handleEditRequest(httpRequest);
        verify(wikiService, times(1)).deleteWikiFile(eq("/old-name"));
        verify(wikiService, times(1)).writeWikiText(eq("/new-name"), notNull());
        verify(htmlService, times(1)).generateRedirectToWikiPage(eq("/new-name"), isNull());
    }

    @Test
    public void testSaveNameCollision() {
        when(wikiService.existsWikiFile(anyString())).thenReturn(true);
        Map<String, String> parameters = new HashMap<>();
        parameters.put("save", "");
        parameters.put("titleeditor", "/new-name");
        parameters.put("contenteditor", "content");
        HttpRequest httpRequest = new HttpRequest(Collections.emptyMap(), HttpRequest.METHOD_GET, "", "/edit/old-name", parameters, new byte[0]);
        editorHandler.handleEditRequest(httpRequest);
        verify(htmlService, times(1)).generateErrorPage(eq(StatusCode.SERVER_INTERNAL_SERVER_ERROR), any(ServiceException.class), anyString(), any());
    }

    @Test
    public void testSaveInvalidName() {
        Map<String, String> parameters = new HashMap<>();
        parameters.put("save", "");
        parameters.put("titleeditor", "");
        HttpRequest httpRequest = new HttpRequest(Collections.emptyMap(), HttpRequest.METHOD_GET, "", "/edit/old-name", parameters, new byte[0]);
        editorHandler.handleEditRequest(httpRequest);
        verify(htmlService, times(1)).generateErrorPage(eq(StatusCode.SERVER_INTERNAL_SERVER_ERROR), any(ServiceException.class), anyString(), any());
    }

    @Test
    public void testShowEditor() throws Exception {
        when(wikiService.existsWikiFile(anyString())).thenReturn(true);
        WikiFile editorWikiFile = new WikiFile("/wiki/Editor", "wiki-editor-content", new WikiPage("/wiki/Editor", null, null, null), new AnyFile("/wiki/Editor.txt"));
        when(wikiService.getWikiFile(eq("/wiki/Editor"))).thenReturn(editorWikiFile);
        HttpRequest httpRequest = buildHttpRequest("/edit/page-name");
        editorHandler.handleEditRequest(httpRequest);
        verify(wikiService, times(1)).getWikiFile(eq("/wiki/Editor"));
    }

    @Test
    public void testFileUploadInvalidName() {
        HttpResponse httpResponse = editorHandler.handleUploadRequest("/upload/", new byte[0]);
        assertEquals(httpResponse.getStatusCode(), StatusCode.CLIENT_BAD_REQUEST);
    }

    @Test
    public void testFileUploadInvalidPath() {
        HttpResponse httpResponse = editorHandler.handleUploadRequest("/upload/path/../with/backward/nav", new byte[0]);
        assertEquals(httpResponse.getStatusCode(), StatusCode.CLIENT_BAD_REQUEST);
    }

    @Test
    public void testFileUploadNameCollision() {
        when(repositoryService.existsFile(eq("/new-name"))).thenReturn(true);
        HttpResponse httpResponse = editorHandler.handleUploadRequest("/upload/new-name", new byte[0]);
        assertEquals(httpResponse.getStatusCode(), StatusCode.SERVER_INTERNAL_SERVER_ERROR);
    }

    @Test
    public void testFileUploadSuccess() throws Exception {
        HttpResponse httpResponse = editorHandler.handleUploadRequest("/upload/new-name", new byte[0]);
        assertEquals(httpResponse.getStatusCode(), StatusCode.OK);
        verify(repositoryService, times(1)).writeBinaryFile(notNull(), notNull(), isNull());
    }

    @Test
    public void testExtractHeadingAnchor() {
        assertNull(EditorHandler.extractHeadingAnchor("text", null, null));
        assertNull(EditorHandler.extractHeadingAnchor("text", 1, null));
        assertNull(EditorHandler.extractHeadingAnchor("text", null, 2));
        assertNull(EditorHandler.extractHeadingAnchor("", 1, 2));
        assertNull(EditorHandler.extractHeadingAnchor("text", 1, 2));
        assertEquals(EditorHandler.extractHeadingAnchor("= text", 1, 2), "text");
        assertEquals(EditorHandler.extractHeadingAnchor("= text =", 1, 2), "text");
        assertEquals(EditorHandler.extractHeadingAnchor("== text ==", 1, 2), "text");
        assertEquals(EditorHandler.extractHeadingAnchor("=== text ===", 1, 2), "text");
        assertEquals(EditorHandler.extractHeadingAnchor("= text =\nmore text", 1, 2), "text");
        assertEquals(EditorHandler.extractHeadingAnchor("= text =\r\nmore text", 1, 2), "text");
        assertEquals(EditorHandler.extractHeadingAnchor("= text with special äöü\"' characters =", 1, 2), "textwithspecialcharacters");
    }

    private HttpRequest buildHttpRequest(@NotNull String urlPath, String... keyValuePairs) {
        Map<String, String> parameters;
        if (keyValuePairs != null) {
            parameters = new HashMap<>();
            for (int i = 0; i + 1 < keyValuePairs.length; i += 2) {
                parameters.put(keyValuePairs[i], keyValuePairs[i + 1]);
            }
        } else {
            parameters = Collections.emptyMap();
        }
        return new HttpRequest(Collections.emptyMap(), HttpRequest.METHOD_GET, "", urlPath, parameters, new byte[0]);
    }
}
