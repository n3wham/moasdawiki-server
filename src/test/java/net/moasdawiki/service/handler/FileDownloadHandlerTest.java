/*
 * MoasdaWiki Server
 *
 * Copyright (C) 2008 - 2023 Herbert Reiter (herbert@moasdawiki.net)
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License version 3 as
 * published by the Free Software Foundation (AGPL-3.0-only).
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

package net.moasdawiki.service.handler;

import net.moasdawiki.base.Logger;
import net.moasdawiki.base.ServiceException;
import net.moasdawiki.base.Settings;
import net.moasdawiki.http.ContentType;
import net.moasdawiki.http.StatusCode;
import net.moasdawiki.service.HttpResponse;
import net.moasdawiki.service.render.HtmlService;
import net.moasdawiki.service.repository.RepositoryService;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.*;
import static org.testng.Assert.*;

public class FileDownloadHandlerTest {

    private RepositoryService repositoryService;
    private HtmlService htmlService;
    private FileDownloadHandler fileDownloadHandler;

    @BeforeMethod
    public void beforeMethod() {
        Logger logger = new Logger(null);
        Settings settings = mock(Settings.class);
        when(settings.getRootPath()).thenReturn("/root");
        repositoryService = mock(RepositoryService.class);
        htmlService = mock(HtmlService.class);
        fileDownloadHandler = new FileDownloadHandler(logger, settings, repositoryService, htmlService);
    }

    @Test
    public void testHandleDownloadImg() throws Exception {
        HttpResponse response = fileDownloadHandler.handleDownloadImg("/img/abc.gif");
        assertEquals(response.getContentType(), ContentType.IMAGE_GIF);
        verify(repositoryService, times(1)).readBinaryFile(argThat(anyFile -> anyFile.getFilePath().equals("/abc.gif")));
    }

    @Test
    public void testHandleDownloadImgFileNotFound() throws Exception {
        when(repositoryService.readBinaryFile(any())).thenThrow(new ServiceException("File not found"));
        fileDownloadHandler.handleDownloadImg("/img/abc.gif");
        verify(htmlService, times(1)).generateErrorPage(eq(StatusCode.CLIENT_NOT_FOUND), anyString(), anyString());
    }

    @Test
    public void testHandleDownloadImgInvalidPath() {
        fileDownloadHandler.handleDownloadImg("/img/../abc.gif");
        verify(htmlService, times(1)).generateErrorPage(eq(StatusCode.CLIENT_FORBIDDEN), anyString(), anyString());
    }

    @Test
    public void testHandleDownloadFile() throws Exception {
        HttpResponse response = fileDownloadHandler.handleDownloadFile("/file/abc.zip");
        assertEquals(response.getContentType(), ContentType.ZIP);
        verify(repositoryService, times(1)).readBinaryFile(argThat(anyFile -> anyFile.getFilePath().equals("/abc.zip")));
    }

    @Test
    public void testHandleDownloadFileFileNotFound() throws Exception {
        when(repositoryService.readBinaryFile(any())).thenThrow(new ServiceException("File not found"));
        fileDownloadHandler.handleDownloadFile("/file/abc.zip");
        verify(htmlService, times(1)).generateErrorPage(eq(StatusCode.CLIENT_NOT_FOUND), anyString(), anyString());
    }

    @Test
    public void testHandleDownloadFileInvalidPath() {
        fileDownloadHandler.handleDownloadFile("/file/../abc.zip");
        verify(htmlService, times(1)).generateErrorPage(eq(StatusCode.CLIENT_FORBIDDEN), anyString(), anyString());
    }

    @Test
    public void testHandleDownloadRootAbsolute() throws Exception {
        HttpResponse response = fileDownloadHandler.handleDownloadRoot("/favicon.ico");
        assertEquals(response.getContentType(), ContentType.IMAGE_ICON);
        verify(repositoryService, times(1)).readBinaryFile(argThat(anyFile -> anyFile.getFilePath().equals("/root/favicon.ico")));
    }

    @Test
    public void testHandleDownloadRootRelative() throws Exception {
        HttpResponse response = fileDownloadHandler.handleDownloadRoot("favicon.ico");
        assertEquals(response.getContentType(), ContentType.IMAGE_ICON);
        verify(repositoryService, times(1)).readBinaryFile(argThat(anyFile -> anyFile.getFilePath().equals("/root/favicon.ico")));
    }

    @Test
    public void testHandleDownloadRootFileNotFound() throws Exception {
        when(repositoryService.readBinaryFile(any())).thenThrow(new ServiceException("File not found"));
        fileDownloadHandler.handleDownloadRoot("/favicon.ico");
        verify(htmlService, times(1)).generateErrorPage(eq(StatusCode.CLIENT_NOT_FOUND), anyString(), anyString());
    }

    @Test
    public void testHandleDownloadRootInvalidPath() {
        fileDownloadHandler.handleDownloadRoot("../favicon.ico");
        verify(htmlService, times(1)).generateErrorPage(eq(StatusCode.CLIENT_FORBIDDEN), anyString(), anyString());
    }
}
