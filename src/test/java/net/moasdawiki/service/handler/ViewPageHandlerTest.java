/*
 * MoasdaWiki Server
 *
 * Copyright (C) 2008 - 2023 Herbert Reiter (herbert@moasdawiki.net)
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License version 3 as
 * published by the Free Software Foundation (AGPL-3.0-only).
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

package net.moasdawiki.service.handler;

import net.moasdawiki.base.Logger;
import net.moasdawiki.base.Settings;
import net.moasdawiki.http.StatusCode;
import net.moasdawiki.service.render.HtmlService;
import net.moasdawiki.service.repository.AnyFile;
import net.moasdawiki.service.wiki.WikiFile;
import net.moasdawiki.service.wiki.WikiService;
import net.moasdawiki.service.wiki.structure.WikiPage;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.*;

public class ViewPageHandlerTest {

    private Settings settings;
    private WikiService wikiService;
    private HtmlService htmlService;
    private ViewPageHandler viewPageHandler;

    @BeforeMethod
    public void beforeMethod() throws Exception {
        Logger logger = new Logger(null);
        settings = mock(Settings.class);
        when(settings.getStartpagePath()).thenReturn("/startpage");
        when(settings.getIndexPageName()).thenReturn("Index");
        when(settings.getIndexFallbackPagePath()).thenReturn("/Index-Template");
        wikiService = mock(WikiService.class);
        when(wikiService.getWikiFile(anyString())).thenReturn(new WikiFile("/path", "content",
                new WikiPage("/path", null, null, null), new AnyFile("/path.txt")));
        htmlService = mock(HtmlService.class);
        viewPageHandler = new ViewPageHandler(logger, settings, wikiService, htmlService);
    }

    @Test
    public void testRootPath() throws Exception {
        viewPageHandler.handleRootPath();
        verify(settings).getStartpagePath();
        verify(wikiService).getWikiFile(eq("/startpage"));
    }

    @Test
    public void testWikiPage() throws Exception {
        viewPageHandler.handleViewPath("/view/page-name");
        verify(wikiService).getWikiFile(eq("/page-name"));
        verify(wikiService, times(1)).addLastViewedWikiFile(anyString());
        verify(htmlService, times(1)).convertPage(notNull());
    }

    @Test
    public void testIndexPageDisabled() {
        when(settings.getIndexPageName()).thenReturn(null);
        viewPageHandler.handleViewPath("/view/wiki/");
        verify(htmlService).generateErrorPage(eq(StatusCode.CLIENT_FORBIDDEN), anyString());
    }

    @Test
    public void testIndexPageLocal() throws Exception {
        when(wikiService.existsWikiFile("/wiki/Index")).thenReturn(true);
        viewPageHandler.handleViewPath("/view/wiki/");
        verify(wikiService, times(1)).getWikiFile(eq("/wiki/Index"));
    }

    @Test
    public void testIndexPageGlobal() throws Exception {
        viewPageHandler.handleViewPath("/view/wiki/");
        verify(wikiService, times(1)).getWikiFile(eq("/Index-Template"));
    }
}
