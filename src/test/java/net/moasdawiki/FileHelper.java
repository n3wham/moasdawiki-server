/*
 * MoasdaWiki Server
 *
 * Copyright (C) 2008 - 2023 Herbert Reiter (herbert@moasdawiki.net)
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License version 3 as
 * published by the Free Software Foundation (AGPL-3.0-only).
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

package net.moasdawiki;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

/**
 * Provides helper methods for file access.
 */
public abstract class FileHelper {

    /**
     * Reads the content of a text file.
     */
    public static String readTextFile(String fileName) throws IOException {
        return new String(Files.readAllBytes(Paths.get(fileName)), StandardCharsets.UTF_8);
    }

    /**
     * Create a temporary directory and copy the files from the given source directory
     * to the new directory.
     */
    public static Path createTempDirectoryAndCopyFiles(String srcDir) throws IOException {
        Path sourceDir = Paths.get(srcDir);
        Path tempDir = Files.createTempDirectory(sourceDir.toFile().getName());
        try (Stream<Path> files = Files.list(sourceDir)) {
            // don't use forEach, so we can throw an Exception
            for (Path file : files.toArray(Path[]::new)) {
                Files.copy(file, tempDir.resolve(sourceDir.relativize(file)));
            }
        }
        return tempDir;
    }

    /**
     * Delete a directory with all its files.
     */
    public static void deleteDirectory(File file) {
        File[] files = file.listFiles();
        if(files != null) {
            for (File f : files) {
                deleteDirectory(f);
            }
        }
        //noinspection ResultOfMethodCallIgnored
        file.delete();
    }
}
