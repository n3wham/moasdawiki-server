/*
 * MoasdaWiki Server
 *
 * Copyright (C) 2008 - 2023 Herbert Reiter (herbert@moasdawiki.net)
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License version 3 as
 * published by the Free Software Foundation (AGPL-3.0-only).
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

package net.moasdawiki.server;

import net.moasdawiki.http.HttpRequest;
import org.testng.annotations.Test;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;

import static org.testng.Assert.*;

public class HttpRequestParserTest {

    @Test
    public void testMethodGet() throws Exception {
        String requestStr = "GET / HTTP/1.1";
        HttpRequest request = HttpRequestParser.parse(new ByteArrayInputStream(requestStr.getBytes(StandardCharsets.UTF_8)));
        assertEquals(request.getMethod(), HttpRequest.METHOD_GET);
    }

    @Test
    public void testMethodPost() throws Exception {
        String requestStr = "POST / HTTP/1.1";
        HttpRequest request = HttpRequestParser.parse(new ByteArrayInputStream(requestStr.getBytes(StandardCharsets.UTF_8)));
        assertEquals(request.getMethod(), HttpRequest.METHOD_POST);
    }

    @Test
    public void testUrl() throws Exception {
        String requestStr = "GET /path/action?key1=value1&key2=value2 HTTP/1.1";
        HttpRequest request = HttpRequestParser.parse(new ByteArrayInputStream(requestStr.getBytes(StandardCharsets.UTF_8)));
        assertEquals(request.getUrl(), "/path/action?key1=value1&key2=value2");
    }

    @Test
    public void testUrlPath() throws Exception {
        String requestStr = "GET /path/action?key1=value1&key2=value2 HTTP/1.1";
        HttpRequest request = HttpRequestParser.parse(new ByteArrayInputStream(requestStr.getBytes(StandardCharsets.UTF_8)));
        assertEquals(request.getUrlPath(), "/path/action");
    }

    @Test
    public void testUrlParameters() throws Exception {
        String requestStr = "GET /path/action?key1=value1&key2=value2 HTTP/1.1";
        HttpRequest request = HttpRequestParser.parse(new ByteArrayInputStream(requestStr.getBytes(StandardCharsets.UTF_8)));
        assertEquals(request.getParameter("key1"), "value1");
        assertEquals(request.getParameter("key2"), "value2");
        assertNull(request.getParameter("key3"));
    }

    @Test
    public void testHttpHeaders() throws Exception {
        String requestStr = "GET / HTTP/1.1\n" +
                "key1: value1\n" +
                "key2: value2\n" +
                "\n";
        HttpRequest request = HttpRequestParser.parse(new ByteArrayInputStream(requestStr.getBytes(StandardCharsets.UTF_8)));
        assertEquals(request.getHttpHeader("key1"), "value1");
        assertEquals(request.getHttpHeader("key2"), "value2");
        assertNull(request.getHttpHeader("key3"));
    }

    @Test
    public void testRawBody() throws Exception {
        String requestStr = "GET / HTTP/1.1\n" +
                "content-length: 9\n" +
                "\n" +
                "body-text";
        HttpRequest request = HttpRequestParser.parse(new ByteArrayInputStream(requestStr.getBytes(StandardCharsets.UTF_8)));
        assertEquals(request.getHttpBody(), "body-text".getBytes(StandardCharsets.UTF_8));
    }

    @Test
    public void testFormData() throws Exception {
        String requestStr = "POST / HTTP/1.1\n" +
                "content-type: application/x-www-form-urlencoded\n" +
                "content-length: 23\n" +
                "\n" +
                "key1=value1&key2=value2";
        HttpRequest request = HttpRequestParser.parse(new ByteArrayInputStream(requestStr.getBytes(StandardCharsets.UTF_8)));
        assertEquals(request.getParameter("key1"), "value1");
        assertEquals(request.getParameter("key2"), "value2");
        assertNull(request.getParameter("key3"));
    }
}
