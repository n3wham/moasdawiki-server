/*
 * MoasdaWiki Server
 *
 * Copyright (C) 2008 - 2023 Herbert Reiter (herbert@moasdawiki.net)
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License version 3 as
 * published by the Free Software Foundation (AGPL-3.0-only).
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

package net.moasdawiki.server;

import net.moasdawiki.http.HttpRequest;
import net.moasdawiki.http.StatusCode;
import net.moasdawiki.service.handler.EditorHandler;
import net.moasdawiki.service.handler.FileDownloadHandler;
import net.moasdawiki.service.handler.SearchHandler;
import net.moasdawiki.service.handler.ViewPageHandler;
import net.moasdawiki.service.render.HtmlService;
import net.moasdawiki.service.sync.SynchronizationService;
import org.jetbrains.annotations.NotNull;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.Map;

import static org.mockito.Mockito.*;

public class RequestDispatcherTest {

    private HtmlService htmlService;
    private ViewPageHandler viewPageHandler;
    private SearchHandler searchHandler;
    private EditorHandler editorHandler;
    private FileDownloadHandler fileDownloadHandler;
    private SynchronizationService synchronizationService;
    private RequestDispatcher requestDispatcher;

    @BeforeMethod
    public void beforeMethod() {
        htmlService = mock(HtmlService.class);
        viewPageHandler = mock(ViewPageHandler.class);
        searchHandler = mock(SearchHandler.class);
        editorHandler = mock(EditorHandler.class);
        fileDownloadHandler = mock(FileDownloadHandler.class);
        synchronizationService = mock(SynchronizationService.class);
        requestDispatcher = new RequestDispatcher(htmlService, viewPageHandler, searchHandler, editorHandler,
                fileDownloadHandler, synchronizationService);
    }

    @Test
    public void testRoot() {
        HttpRequest httpRequest = buildHttpRequest("/");
        requestDispatcher.handleRequest(httpRequest);
        verify(viewPageHandler, times(1)).handleRootPath();
    }

    @Test
    public void testView() {
        HttpRequest httpRequest = buildHttpRequest("/view/abc");
        requestDispatcher.handleRequest(httpRequest);
        verify(viewPageHandler, times(1)).handleViewPath(eq("/view/abc"));
    }

    @Test
    public void testSearch() {
        HttpRequest httpRequest = buildHttpRequest("/search/", "text", "abc");
        requestDispatcher.handleRequest(httpRequest);
        verify(searchHandler, times(1)).handleSearchRequest(eq("abc"));
    }

    @Test
    public void testEdit() {
        HttpRequest httpRequest = buildHttpRequest("/edit/abc");
        requestDispatcher.handleRequest(httpRequest);
        verify(editorHandler, times(1)).handleEditRequest(same(httpRequest));
    }

    @Test
    public void testUpload() {
        HttpRequest httpRequest = buildHttpRequest("/upload");
        requestDispatcher.handleRequest(httpRequest);
        verify(editorHandler, times(1)).handleUploadRequest(eq("/upload"), argThat(httpBody -> httpBody.length == 0));
    }

    @Test
    public void testSync() {
        HttpRequest httpRequest = buildHttpRequest("/sync/create-session");
        requestDispatcher.handleRequest(httpRequest);
        verify(synchronizationService, times(1)).handleSyncRequest(same(httpRequest));
    }

    @SuppressWarnings("ConstantConditions")
    @Test
    public void testSyncNull() {
        synchronizationService = null;
        requestDispatcher = new RequestDispatcher(htmlService, viewPageHandler, searchHandler, editorHandler,
                fileDownloadHandler, synchronizationService);
        HttpRequest httpRequest = buildHttpRequest("/sync/create-session");
        requestDispatcher.handleRequest(httpRequest);
        verify(htmlService, times(1)).generateErrorPage(eq(StatusCode.CLIENT_NOT_FOUND), anyString(), anyString());
    }

    @Test
    public void testImageDownload() {
        HttpRequest httpRequest = buildHttpRequest("/img/");
        requestDispatcher.handleRequest(httpRequest);
        verify(fileDownloadHandler, times(1)).handleDownloadImg(eq("/img/"));
    }

    @Test
    public void testFileDownload() {
        HttpRequest httpRequest = buildHttpRequest("/file/");
        requestDispatcher.handleRequest(httpRequest);
        verify(fileDownloadHandler, times(1)).handleDownloadFile(eq("/file/"));
    }

    @Test
    public void testRootFileDownload() {
        HttpRequest httpRequest = buildHttpRequest("/file");
        requestDispatcher.handleRequest(httpRequest);
        verify(fileDownloadHandler, times(1)).handleDownloadRoot(eq("/file"));
    }

    @Test
    public void testUnknownPath() {
        HttpRequest httpRequest = buildHttpRequest("/unknown/");
        requestDispatcher.handleRequest(httpRequest);
        verify(htmlService, times(1)).generateErrorPage(eq(StatusCode.CLIENT_NOT_FOUND), anyString(), eq("/unknown/"));
    }

    private HttpRequest buildHttpRequest(@NotNull String urlPath) {
        return new HttpRequest(Collections.emptyMap(), HttpRequest.METHOD_GET, "", urlPath, Collections.emptyMap(), new byte[0]);
    }

    @SuppressWarnings("SameParameterValue")
    private HttpRequest buildHttpRequest(@NotNull String urlPath, @NotNull String paramKey, @NotNull String paramValue) {
        Map<String, String> parameters = Collections.singletonMap(paramKey, paramValue);
        return new HttpRequest(Collections.emptyMap(), HttpRequest.METHOD_GET, "", urlPath, parameters, new byte[0]);
    }
}
