/*
 * MoasdaWiki Server
 *
 * Copyright (C) 2008 - 2023 Herbert Reiter (herbert@moasdawiki.net)
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License version 3 as
 * published by the Free Software Foundation (AGPL-3.0-only).
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

package net.moasdawiki.http;

import org.testng.annotations.Test;

import static org.testng.Assert.*;

/**
 * Tests for class StatusCodeUtils.
 */
public class StatusCodeUtilsTest {

    @Test
    public void testContentTypeByFileSuffix() {
        assertEquals(StatusCodeUtils.contentTypeByFileSuffix(""), ContentType.BINARY);
        assertEquals(StatusCodeUtils.contentTypeByFileSuffix("abc"), ContentType.BINARY);
        assertEquals(StatusCodeUtils.contentTypeByFileSuffix("file.css"), ContentType.CSS);
        assertEquals(StatusCodeUtils.contentTypeByFileSuffix("file.CSS"), ContentType.CSS);
        assertEquals(StatusCodeUtils.contentTypeByFileSuffix("file.gif"), ContentType.IMAGE_GIF);
        assertEquals(StatusCodeUtils.contentTypeByFileSuffix("file.htm"), ContentType.HTML);
        assertEquals(StatusCodeUtils.contentTypeByFileSuffix("file.html"), ContentType.HTML);
        assertEquals(StatusCodeUtils.contentTypeByFileSuffix("file.ico"), ContentType.IMAGE_ICON);
        assertEquals(StatusCodeUtils.contentTypeByFileSuffix("file.jpg"), ContentType.IMAGE_JPEG);
        assertEquals(StatusCodeUtils.contentTypeByFileSuffix("file.jpeg"), ContentType.IMAGE_JPEG);
        assertEquals(StatusCodeUtils.contentTypeByFileSuffix("file.png"), ContentType.IMAGE_PNG);
        assertEquals(StatusCodeUtils.contentTypeByFileSuffix("file.pdf"), ContentType.PDF);
        assertEquals(StatusCodeUtils.contentTypeByFileSuffix("file.svg"), ContentType.IMAGE_SVG);
        assertEquals(StatusCodeUtils.contentTypeByFileSuffix("file.txt"), ContentType.PLAIN);
        assertEquals(StatusCodeUtils.contentTypeByFileSuffix("file.zip"), ContentType.ZIP);
    }
}
