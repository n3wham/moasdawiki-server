/*
 * MoasdaWiki Server
 *
 * Copyright (C) 2008 - 2023 Herbert Reiter (herbert@moasdawiki.net)
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License version 3 as
 * published by the Free Software Foundation (AGPL-3.0-only).
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

package net.moasdawiki.service.sync;

import net.moasdawiki.base.Logger;
import net.moasdawiki.base.ServiceException;
import net.moasdawiki.base.Settings;
import net.moasdawiki.http.ContentType;
import net.moasdawiki.http.HttpRequest;
import net.moasdawiki.http.StatusCode;
import net.moasdawiki.service.HttpResponse;
import net.moasdawiki.service.repository.AnyFile;
import net.moasdawiki.service.repository.RepositoryService;
import net.moasdawiki.util.DateUtils;
import net.moasdawiki.util.EscapeUtils;
import net.moasdawiki.util.JavaScriptUtils;
import net.moasdawiki.util.StringUtils;
import net.moasdawiki.util.xml.XmlGenerator;
import net.moasdawiki.util.xml.XmlParser;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.BufferedReader;
import java.io.StringReader;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.util.*;

/**
 * Provides a webservice endpoint to synchronize the repository content with
 * the MoasdaWiki App.
 *
 * Uses the server system time as time reference, never the client time, to
 * avoid unexpected effects.
 *
 * See the method documentation for more information about URLs and data syntax.
 */
public class SynchronizationService {

	/**
	 * Session list file.
	 */
	public static final String SESSION_LIST_FILEPATH = "/syncsessions.config";

	/**
	 * Endpoint protocol version.
	 */
	private static final String PROTOCOL_VERSION = "2.0";

	/**
	 * Upper transfer file size limit to avoid an OutOfMemoryException in the App.
	 */
	private static final int MAX_READ_FILE_SIZE = 10 * 1000 * 1000; // 10 MB

	/**
	 * File paths to be excluded from synchronization.
	 */
	private static final String[] EXCLUDE_FILEPATHS = { SESSION_LIST_FILEPATH };

	@NotNull
	private final Logger logger;

	@NotNull
	private final Settings settings;

	@NotNull
	private final RepositoryService repositoryService;

	@NotNull
	private final SecureRandom random;

	/**
	 * Map: Session ID --> session data.
	 */
	@NotNull
	private final Map<String, SessionData> sessionMap;

	/**
	 * Constructor.
	 */
	public SynchronizationService(@NotNull Logger logger, @NotNull Settings settings,
								  @NotNull RepositoryService repositoryService) {
		this.logger = logger;
		this.settings = settings;
		this.repositoryService = repositoryService;
		this.random = new SecureRandom();
		this.sessionMap = new HashMap<>();
		readSessionList();
	}

	/**
	 * Returns the session list.
	 */
	@NotNull
	public List<SessionData> getSessions() {
		return new ArrayList<>(sessionMap.values());
	}

	/**
	 * Return the session data for a single session ID.
	 * Convenient method for testing only.
	 */
	SessionData getSession(@NotNull String sessionId) {
		return sessionMap.get(sessionId);
	}

	/**
	 * Handles all sync requests.
	 */
	@Nullable
	public HttpResponse handleSyncRequest(@NotNull HttpRequest httpRequest) {
		switch (httpRequest.getUrlPath()) {
			case "/sync-gui/session-permit": {
				String sessionId = httpRequest.getParameter("session-id");
				return handleSessionPermit(sessionId);
			}
			case "/sync-gui/session-drop": {
				String sessionId = httpRequest.getParameter("session-id");
				return handleSessionDrop(sessionId);
			}
			case "/sync/create-session": {
				return handleCreateSession(httpRequest.getHttpBody());
			}
			case "/sync/check-session": {
				return handleCheckSession(httpRequest.getHttpBody());
			}
			case "/sync/list-modified-files": {
				return handleListModifiedFiles(httpRequest.getHttpBody());
			}
			case "/sync/read-file": {
				return handleReadFile(httpRequest.getHttpBody());
			}
			default:
				return null;
		}
	}

	/**
	 * Permits access for a App session. This enables the App to synchronize
	 * the repository content.
	 *
	 * Is called via user request on the wiki server synchronization page.
	 *
	 * URL path: /sync-gui/session-permit (GET)
	 *
	 * URL parameter:
	 * session-id: Session ID
	 *
	 * JSON response:
	 *
	 * <pre>
	 * {
	 *   'code': 0 = ok / 1 = error,
	 *   'message': 'error message'
	 * }
	 * </pre>
	 */
	@NotNull
	private HttpResponse handleSessionPermit(@Nullable String sessionId) {
		if (sessionId == null) {
			return generateJsonResponse(1, "Parameter session-id missing");
		}
		SessionData sessionData = sessionMap.get(sessionId);
		if (sessionData == null) {
			return generateJsonResponse(1, "Session unknown: " + sessionId);
		}
		sessionData.authorized = true;
		writeSessionList();
		return generateJsonResponse(0);
	}

	/**
	 * Deletes a session from the session list. Afterwards, the corresponding
	 * App cannot synchronize the repository content any more.
	 *
	 * Is called via user request on the wiki server synchronization page.
	 *
	 * URL path: /sync-gui/session-drop (GET)
	 *
	 * URL parameter:
	 * session-id: Session ID
	 *
	 * JSON response:
	 * 
	 * <pre>
	 * {
	 *   'code': 0 = ok / 1 = error,
	 *   'message': 'error message'
	 * }
	 * </pre>
	 */
	@NotNull
	private HttpResponse handleSessionDrop(@Nullable String sessionId) {
		if (sessionId == null) {
			return generateJsonResponse(1, "Parameter session-id missing");
		}
		SessionData sessionData = sessionMap.remove(sessionId);
		if (sessionData == null) {
			return generateJsonResponse(1, "Session unknown: " + sessionId);
		}
		writeSessionList();
		return generateJsonResponse(0);
	}

	@SuppressWarnings("SameParameterValue")
	private HttpResponse generateJsonResponse(int code) {
		return generateJsonResponse(code, null);
	}

	private HttpResponse generateJsonResponse(int code, @Nullable String jsonText) {
		String contentString = JavaScriptUtils.generateJsonCodeAndMessage(code, jsonText);
		byte[] content = StringUtils.stringToUtf8Bytes(contentString);
		return new HttpResponse(StatusCode.OK, ContentType.JSON_UTF8, content);
	}

	/**
	 * Creates a new App session.
	 *
	 * Is called from a MoasdaWiki App to generate a server session. The
	 * session has to be authorized manually by the user before it can be used
	 * to synchronize the repository content.
	 *
	 * Additionally, the server stores the client session id. This enables the
	 * App to check if it is still connected to the same server instance.
	 *
	 * This handshake mechanism allows to uniquely identify an App instance and
	 * secures the server.
	 *
	 * URL path: /sync/create-session (POST)
	 *
	 * POST data:
	 * 
	 * <pre>
	 * &lt;?xml version="1.0" encoding="UTF-8"?>
	 * &lt;create-session version="2.0">
	 *   &lt;client-session-id>abcdefghi&lt;/client-session-id>
	 *   &lt;client-name>MoasdaWiki App&lt;/client-name>
	 *   &lt;client-version>2.1&lt;/client-version>
	 *   &lt;client-host>ANDROIDPHONE1&lt;/client-host>
	 * &lt;/create-session>
	 * </pre>
	 *
	 * HTTP response:
	 * 
	 * <pre>
	 * &lt;?xml version="1.0" encoding="UTF-8"?>
	 * &lt;create-session-response version="2.0">
	 *   &lt;server-session-id>1234567890&lt;/server-session-id>
	 *   &lt;server-name>MoasdaWiki&lt;/server-name>
	 *   &lt;server-version>2.0&lt;/server-version>
	 *   &lt;server-host>SERVERNAME1&lt;/server-host>
	 * &lt;/create-session-response>
	 * </pre>
	 */
	private HttpResponse handleCreateSession(byte @NotNull [] httpBody) {
		try {
			String requestXml = fromUtf8Bytes(httpBody);
			logger.write("Received XML: " + requestXml);
			CreateSessionXml createSession = parseXml(requestXml, CreateSessionXml.class);

			SessionData sessionData = new SessionData();
			sessionData.serverSessionId = generateSessionId();
			sessionData.clientSessionId = createSession.clientSessionId;
			sessionData.createTimestamp = new Date();
			sessionData.clientName = createSession.clientName;
			sessionData.clientVersion = createSession.clientVersion;
			sessionData.clientHost = createSession.clientHost;
			sessionMap.put(sessionData.serverSessionId, sessionData);
			writeSessionList();

			CreateSessionResponseXml createSessionResponseXml = new CreateSessionResponseXml();
			createSessionResponseXml.version = PROTOCOL_VERSION;
			createSessionResponseXml.serverSessionId = sessionData.serverSessionId;
			createSessionResponseXml.serverName = settings.getProgramName();
			createSessionResponseXml.serverVersion = settings.getVersion();
			createSessionResponseXml.serverHost = settings.getServerHost();
			String responseXml = generateXml(createSessionResponseXml);
			logger.write("Sending XML: " + responseXml);

			byte[] content = StringUtils.stringToUtf8Bytes(responseXml);
			return new HttpResponse(StatusCode.OK, ContentType.XML, content);
		} catch (ServiceException e) {
			String msg = "Error generating synchronization session";
			logger.write(msg, e);
			return generateErrorResponse(msg);
		}
	}

	private String generateSessionId() {
		return new BigInteger(130, random).toString(32);
	}

	/**
	 * Checks the server session ID and returns if the session has already been
	 * permitted for repository synchronization.
	 *
	 * URL path: /sync/check-session (POST)
	 *
	 * POST data:
	 * 
	 * <pre>
	 * &lt;?xml version="1.0" encoding="UTF-8"?>
	 * &lt;check-session version="2.0">
	 *   &lt;server-session-id>1234567890&lt;/server-session-id>
	 * &lt;/check-session>
	 * </pre>
	 *
	 * HTTP response:
	 * 
	 * <pre>
	 * &lt;?xml version="1.0" encoding="UTF-8"?>
	 * &lt;check-session-response version="2.0">
	 *   &lt;valid>true&lt;/valid>
	 *   &lt;authorized>true&lt;/authorized>
	 *   &lt;client-session-id>abcdefghi&lt;/client-session-id>
	 * &lt;/check-session-response>
	 * </pre>
	 */
	private HttpResponse handleCheckSession(byte @NotNull [] httpBody) {
		try {
			String requestXml = fromUtf8Bytes(httpBody);
			logger.write("Received XML: " + requestXml);
			CheckSessionXml checkSession = parseXml(requestXml, CheckSessionXml.class);

			if (checkSession.serverSessionId == null) {
				return generateErrorResponse("Parameter server-session-id missing");
			}
			SessionData sessionData = sessionMap.get(checkSession.serverSessionId);

			CheckSessionResponseXml checkSessionResponseXml = new CheckSessionResponseXml();
			checkSessionResponseXml.version = PROTOCOL_VERSION;
			checkSessionResponseXml.valid = (sessionData != null);
			if (sessionData != null) {
				checkSessionResponseXml.authorized = sessionData.authorized;
				checkSessionResponseXml.clientSessionId = sessionData.clientSessionId;
			}
			String responseXml = generateXml(checkSessionResponseXml);
			logger.write("Sending XML: " + responseXml);

			byte[] content = StringUtils.stringToUtf8Bytes(responseXml);
			return new HttpResponse(StatusCode.OK, ContentType.XML, content);
		} catch (ServiceException e) {
			String msg = "Error generating synchronization session";
			logger.write(msg, e);
			return generateErrorResponse(msg);
		}
	}

	/**
	 * Lists all repository files that were modified since the last synchronization date.
	 *
	 * URL path: /sync/list-modified-files (POST)
	 *
	 * POST data:
	 * 
	 * <pre>
	 * &lt;?xml version="1.0" encoding="UTF-8"?>
	 * &lt;list-modified-files version="2.0">
	 *   &lt;server-session-id>1234567890&lt;/server-session-id>
	 *   &lt;last-sync-server-time>2015-03-01T10:00:00.000Z&lt;/last-sync-server-time>
	 * &lt;/list-modified-files>
	 * </pre>
	 * 
	 * HTTP response:
	 * 
	 * <pre>
	 * &lt;?xml version="1.0" encoding="UTF-8"?>
	 * &lt;list-modified-files-response version="2.0" current-server-time="2015-03-01T10:00:00.000Z">
	 *   &lt;file timestamp="2015-02-25T15:30:00.000Z">/Startpage.txt&lt;/file>
	 *   &lt;file timestamp="2015-02-23T11:45:00.000Z">/image.png&lt;/file>
	 * &lt;/list-modified-files-response>
	 * </pre>
	 */
	@NotNull
	private HttpResponse handleListModifiedFiles(byte @NotNull [] httpBody) {
		try {
			String requestXml = fromUtf8Bytes(httpBody);
			logger.write("Received XML: " + requestXml);
			ListModifiedFilesXml listModifiedFiles = parseXml(requestXml, ListModifiedFilesXml.class);

			if (listModifiedFiles.serverSessionId == null) {
				return generateErrorResponse("Parameter server-session-id missing");
			}
			SessionData sessionData = sessionMap.get(listModifiedFiles.serverSessionId);
			if (sessionData == null) {
				return generateErrorResponse("Session unknown: " + listModifiedFiles.serverSessionId);
			} else if (!sessionData.authorized) {
				return generateErrorResponse("Session not authorized: " + listModifiedFiles.serverSessionId);
			}
			sessionData.lastSyncTimestamp = new Date();

			Date lastSyncServerTime = parseUtcDateOrNull(listModifiedFiles.lastSyncServerTime);
			Set<AnyFile> files = getModifiedFiles(lastSyncServerTime);

			ListModifiedFilesResponseXml xmlResponse = new ListModifiedFilesResponseXml();
			xmlResponse.version = PROTOCOL_VERSION;
			xmlResponse.currentServerTime = DateUtils.formatUtcDate(new Date());
			xmlResponse.fileList = new ArrayList<>();
			for (AnyFile file : files) {
				SingleFileXml singleFile = new SingleFileXml();
				singleFile.timestamp = DateUtils.formatUtcDate(file.getContentTimestamp());
				singleFile.filePath = file.getFilePath();
				xmlResponse.fileList.add(singleFile);
			}

			String responseXml = generateXml(xmlResponse);
			logger.write("Sending XML: " + responseXml);

			byte[] content = StringUtils.stringToUtf8Bytes(responseXml);
			return new HttpResponse(StatusCode.OK, ContentType.XML, content);
		} catch (ServiceException e) {
			String msg = "Error sending modified files list";
			logger.write(msg, e);
			return generateErrorResponse(msg);
		}
	}

	/**
	 * Returns the repository files that were modified since the given date.
	 */
	private Set<AnyFile> getModifiedFiles(Date lastSyncServerTime) {
		Set<AnyFile> filesModified = repositoryService.getModifiedAfter(lastSyncServerTime);
		for (String filePath : EXCLUDE_FILEPATHS) {
			filesModified.remove(new AnyFile(filePath));
		}
		return filesModified;
	}

	/**
	 * Downloads a single file from the server repository.
	 *
	 * URL path: /sync/read-file (POST)
	 *
	 * POST data:
	 * 
	 * <pre>
	 * &lt;?xml version="1.0" encoding="UTF-8"?>
	 * &lt;read-file version="2.0">
	 *   &lt;server-session-id>1234567890&lt;/server-session-id>
	 *   &lt;file-path>/Startpage.txt&lt;/file-path>
	 * &lt;/read-file>
	 * </pre>
	 * 
	 * HTTP response:
	 * 
	 * <pre>
	 * &lt;?xml version="1.0" encoding="UTF-8"?>
	 * &lt;read-file-response version="2.0">
	 *   &lt;timestamp>2015-02-25T15:30:00.000Z&lt;/timestamp>
	 *   &lt;content>base64-encoded file content...&lt;/content>
	 * &lt;/read-file-response>
	 * </pre>
	 */
	@NotNull
	private HttpResponse handleReadFile(byte @NotNull [] httpBody) {
		try {
			String requestXml = fromUtf8Bytes(httpBody);
			logger.write("Received XML: " + requestXml);
			ReadFileXml readFileXml = parseXml(requestXml, ReadFileXml.class);

			if (readFileXml.serverSessionId == null) {
				return generateErrorResponse("Parameter server-session-id missing");
			}
			SessionData sessionData = sessionMap.get(readFileXml.serverSessionId);
			if (sessionData == null) {
				return generateErrorResponse("Session unknown: " + readFileXml.serverSessionId);
			} else if (!sessionData.authorized) {
				return generateErrorResponse("Session not authorized: " + readFileXml.serverSessionId);
			}

			AnyFile anyFile = repositoryService.getFile(readFileXml.filePath);
			if (anyFile == null) {
				return generateErrorResponse("File not found: " + readFileXml.filePath);
			}
			byte[] fileContent = repositoryService.readBinaryFile(anyFile);

			if (fileContent.length > MAX_READ_FILE_SIZE) {
				return generateErrorResponse("File size too big for synchronization: " + readFileXml.filePath);
			}

			ReadFileResponseXml xmlResponse = new ReadFileResponseXml();
			xmlResponse.version = PROTOCOL_VERSION;
			xmlResponse.timestamp = DateUtils.formatUtcDate(anyFile.getContentTimestamp());
			xmlResponse.content = EscapeUtils.encodeBase64(fileContent);
			String responseXml = generateXml(xmlResponse);
			logger.write("Sending XML: " + truncateLogText(responseXml));
			byte[] content = StringUtils.stringToUtf8Bytes(responseXml);
			return new HttpResponse(StatusCode.OK, ContentType.XML, content);
		} catch (ServiceException e) {
			String msg = "Error reading file content";
			logger.write(msg, e);
			return generateErrorResponse(msg);
		}
	}

	private String truncateLogText(String logText) {
		if (logText.length() <= 200) {
			return logText;
		}
		return logText.substring(0, 200) + '…';
	}

	/**
	 * Generates a HTTP error response.
	 */
	@NotNull
	private HttpResponse generateErrorResponse(@NotNull String message) {
		try {
			ErrorResponseXml errorResponseXml = new ErrorResponseXml();
			errorResponseXml.version = PROTOCOL_VERSION;
			errorResponseXml.message = message;
			String responseXml = generateXml(errorResponseXml);
			logger.write("Sending error XML: " + responseXml);

			byte[] content = StringUtils.stringToUtf8Bytes(responseXml);
			return new HttpResponse(StatusCode.OK, ContentType.XML, content);
		} catch (ServiceException e) {
			logger.write("Cannot generate error response XML, sending 500", e);
			return new HttpResponse(StatusCode.SERVER_INTERNAL_SERVER_ERROR);
		}
	}

	@Nullable
	private static Date parseUtcDateOrNull(@NotNull String dateStr) {
		try {
			return DateUtils.parseUtcDate(dateStr);
		} catch (ServiceException e) {
			return null;
		}
	}

	/**
	 * Converts a JAXB bean into a XML string.
	 */
	@NotNull
	private String generateXml(@NotNull AbstractSyncXml xmlBean) throws ServiceException {
		XmlGenerator xmlGenerator = new XmlGenerator();
		return xmlGenerator.generate(xmlBean);
	}

	/**
	 * Converts a XML string into a JAXB bean.
	 */
	@NotNull
	private <T extends AbstractSyncXml> T parseXml(@NotNull String xml, @NotNull Class<T> xmlBeanType) throws ServiceException {
		XmlParser xmlParser = new XmlParser(logger);
		return xmlParser.parse(xml, xmlBeanType);
	}

	@NotNull
	private static String fromUtf8Bytes(byte @NotNull [] bytes) {
		return new String(bytes, StandardCharsets.UTF_8);
	}

	/**
	 * Reads the persisted session list from a file.
	 */
	private void readSessionList() {
		AnyFile sessionListFile = repositoryService.getFile(SESSION_LIST_FILEPATH);
		if (sessionListFile == null) {
			// Datei existiert nicht, keinen Stacktrace loggen, nur Notiz
			logger.write("Session list file not found: " + SESSION_LIST_FILEPATH);
			return;
		}

		try {
			String fileContent = repositoryService.readTextFile(sessionListFile);
			BufferedReader reader = new BufferedReader(new StringReader(fileContent));
			String line;
			int count = 0;
			while ((line = reader.readLine()) != null) {
				String[] token = line.split("\t", 8);
				if (token.length < 7) {
					// Zeile ignorieren
					continue;
				}

				SessionData sessionData = new SessionData();
				sessionData.serverSessionId = StringUtils.emptyToNull(token[0]);
				sessionData.clientSessionId = StringUtils.emptyToNull(token[1]);
				sessionData.createTimestamp = DateUtils.parseUtcDate(StringUtils.emptyToNull(token[2]));
				sessionData.clientName = StringUtils.emptyToNull(token[3]);
				sessionData.clientVersion = StringUtils.emptyToNull(token[4]);
				sessionData.clientHost = StringUtils.emptyToNull(token[5]);
				sessionData.authorized = Boolean.parseBoolean(token[6]);
				sessionMap.put(sessionData.serverSessionId, sessionData);
				count++;
			}
			reader.close();
			logger.write(count + " sessions read");
		} catch (Exception e) {
			logger.write("Error reading session list file " + SESSION_LIST_FILEPATH, e);
		}
	}

	/**
	 * Persists the session list in a file.
	 */
	private void writeSessionList() {
		StringBuilder sb = new StringBuilder();
		for (String serverSessionId : sessionMap.keySet()) {
			SessionData sessionData = sessionMap.get(serverSessionId);
			sb.append(StringUtils.nullToEmpty(sessionData.serverSessionId));
			sb.append('\t');
			sb.append(StringUtils.nullToEmpty(sessionData.clientSessionId));
			sb.append('\t');
			sb.append(StringUtils.nullToEmpty(DateUtils.formatUtcDate(sessionData.createTimestamp)));
			sb.append('\t');
			sb.append(StringUtils.nullToEmpty(sessionData.clientName));
			sb.append('\t');
			sb.append(StringUtils.nullToEmpty(sessionData.clientVersion));
			sb.append('\t');
			sb.append(StringUtils.nullToEmpty(sessionData.clientHost));
			sb.append('\t');
			sb.append(sessionData.authorized);
			sb.append('\n');
		}
		String fileContent = sb.toString();

		// Datei schreiben
		try {
			AnyFile anyFile = new AnyFile(SESSION_LIST_FILEPATH);
			repositoryService.writeTextFile(anyFile, fileContent);
			logger.write(sessionMap.size() + " sessions written");
		} catch (ServiceException e) {
			logger.write("Error writing session list file " + SESSION_LIST_FILEPATH, e);
		}
	}
}
