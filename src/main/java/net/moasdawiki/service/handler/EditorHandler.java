/*
 * MoasdaWiki Server
 *
 * Copyright (C) 2008 - 2023 Herbert Reiter (herbert@moasdawiki.net)
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License version 3 as
 * published by the Free Software Foundation (AGPL-3.0-only).
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

package net.moasdawiki.service.handler;

import net.moasdawiki.base.Logger;
import net.moasdawiki.base.Messages;
import net.moasdawiki.base.ServiceException;
import net.moasdawiki.base.Settings;
import net.moasdawiki.http.ContentType;
import net.moasdawiki.http.HttpRequest;
import net.moasdawiki.http.StatusCode;
import net.moasdawiki.service.HttpResponse;
import net.moasdawiki.service.render.HtmlService;
import net.moasdawiki.service.repository.AnyFile;
import net.moasdawiki.service.repository.RepositoryService;
import net.moasdawiki.service.wiki.WikiFile;
import net.moasdawiki.service.wiki.WikiHelper;
import net.moasdawiki.service.wiki.WikiService;
import net.moasdawiki.service.wiki.WikiText;
import net.moasdawiki.service.wiki.structure.WikiPage;
import net.moasdawiki.util.EscapeUtils;
import net.moasdawiki.util.JavaScriptUtils;
import net.moasdawiki.util.PathUtils;
import net.moasdawiki.util.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.nio.charset.StandardCharsets;
import java.util.*;

/**
 * Text editor to edit a wiki page.
 */
public class EditorHandler {

    private static final String ERROR_KEY = "EditorHandler.error";
    private static final String DELETE_ERROR_KEY = "EditorHandler.delete.error";
    private static final String DELETE_CONFIRMATION_KEY = "EditorHandler.delete.confirmation";
    private static final String SAVE_INVALID_NAME_KEY = "EditorHandler.save.invalidName";
    private static final String SAVE_ALREADY_EXISTING_KEY = "EditorHandler.save.alreadyExisting";
    private static final String SAVE_ERROR_KEY = "EditorHandler.save.error";
    private static final String EDITOR_ERROR_KEY = "EditorHandler.editor.error";
    private static final String EDITOR_TITLE_NEW_PAGE_KEY = "EditorHandler.editor.title.newPage";
    private static final String EDITOR_INPUT_TITLE_KEY = "EditorHandler.editor.input.title";
    private static final String EDITOR_INPUT_CONTENT_KEY = "EditorHandler.editor.input.content";
    private static final String EDITOR_INPUT_SAVE_KEY = "EditorHandler.editor.input.save";
    private static final String EDITOR_INPUT_CANCEL_KEY = "EditorHandler.editor.input.cancel";
    private static final String EDITOR_INPUT_DELETE_KEY = "EditorHandler.editor.input.delete";
    private static final String EDITOR_INPUT_TEMPLATE_SELECT_KEY = "EditorHandler.editor.input.templateSelect";
    private static final String EDITOR_INPUT_UPLOAD_HINT_KEY = "EditorHandler.editor.input.upload.hint";
    private static final String EDITOR_INPUT_UPLOAD_TITLE_KEY = "EditorHandler.editor.input.upload.title";
    private static final String EDITOR_HELP_KEY = "EditorHandler.editor.help";
    private static final String EDITOR_UPLOADPANEL_TITLE_KEY = "EditorHandler.editor.uploadPanel.title";
    private static final String EDITOR_UPLOADPANEL_FILE_KEY = "EditorHandler.editor.uploadPanel.file";
    private static final String EDITOR_UPLOADPANEL_REPOSITORY_PATH_KEY = "EditorHandler.editor.uploadPanel.repositoryPath";
    private static final String EDITOR_UPLOADPANEL_IMAGE_TAG_KEY = "EditorHandler.editor.uploadPanel.imageTag";
    private static final String EDITOR_UPLOADPANEL_FILE_TAG_KEY = "EditorHandler.editor.uploadPanel.fileTag";
    private static final String EDITOR_UPLOADPANEL_SAVE_KEY = "EditorHandler.editor.uploadPanel.save";
    private static final String EDITOR_UPLOADPANEL_CANCEL_KEY = "EditorHandler.editor.uploadPanel.cancel";
    private static final String UPLOAD_NO_FILE_SELECTED_KEY = "EditorHandler.upload.no-file-selected";
    private static final String UPLOAD_MULTIPLE_FILES_SELECTED_KEY = "EditorHandler.upload.multiple-files-selected";
    private static final String UPLOAD_FILE_TOO_BIG_KEY = "EditorHandler.upload.file-too-big";
    private static final String UPLOAD_INVALID_NAME_KEY = "EditorHandler.upload.invalidName";
    private static final String UPLOAD_PARENT_NAVIGATION_KEY = "EditorHandler.upload.parentNavigation";
    private static final String UPLOAD_ALREADY_EXISTING_KEY = "EditorHandler.upload.alreadyExisting";

    private final Logger logger;
    private final Settings settings;
    private final Messages messages;
    private final RepositoryService repositoryService;
    private final WikiService wikiService;
    private final HtmlService htmlService;

    /**
     * Constructor.
     */
    public EditorHandler(@NotNull Logger logger, @NotNull Settings settings, @NotNull Messages messages,
                         @NotNull RepositoryService repositoryService, @NotNull WikiService wikiService,
                         @NotNull HtmlService htmlService) {
        this.logger = logger;
        this.settings = settings;
        this.messages = messages;
        this.repositoryService = repositoryService;
        this.wikiService = wikiService;
        this.htmlService = htmlService;
    }

    /**
     * Handles all requests regarding the editor page.
     * <p>
     * URL: /edit/pagename
     */
    @NotNull
    public HttpResponse handleEditRequest(@NotNull HttpRequest httpRequest) {
        try {
            // cut off "/edit/" prefix
            String pagePath = httpRequest.getUrlPath().substring(5);
            pagePath = EscapeUtils.url2PagePath(pagePath);

            if (httpRequest.getParameter("json") != null) {
                // JavaScript loads JSON content for editor page
                Integer fromPos = StringUtils.parseInteger(httpRequest.getParameter("fromPos"));
                Integer toPos = StringUtils.parseInteger(httpRequest.getParameter("toPos"));
                return generateEditorContentJson(pagePath, fromPos, toPos);

            } else if (httpRequest.getParameter("cancel") != null) {
                // user has clicked cancel button
                return cancelEditing(pagePath);

            } else if (httpRequest.getParameter("delete") != null) {
                // user has clicked delete button
                return deleteWikiPage(pagePath);

            } else if (httpRequest.getParameter("save") != null) {
                // user has clicked save button
                String newPagePath = httpRequest.getParameter("titleeditor");
                if (newPagePath == null) {
                    newPagePath = ""; // leads to invalid page name error
                }
                String newWikiText = httpRequest.getParameter("contenteditor");
                Integer fromPos = StringUtils.parseInteger(httpRequest.getParameter("fromPos"));
                Integer toPos = StringUtils.parseInteger(httpRequest.getParameter("toPos"));
                return saveWikiText(pagePath, newPagePath, newWikiText, fromPos, toPos);

            } else {
                // No action parameters -> show editor page
                return showEditor(pagePath);
            }
        } catch (ServiceException e) {
            return htmlService.generateErrorPage(StatusCode.SERVER_INTERNAL_SERVER_ERROR, e, ERROR_KEY, e.getMessage());
        }
    }

    /**
     * Returns the dynamic content of the editor page as JSON object.
     */
    @NotNull
    private HttpResponse generateEditorContentJson(@NotNull String pagePath, @Nullable Integer fromPos, @Nullable Integer toPos) throws ServiceException {
        Map<String, Object> pageContentMap = new HashMap<>();
        String formPath = PathUtils.concatWebPaths("/edit/", pagePath);
        pageContentMap.put("page-path", "/".equals(pagePath) ? "" : pagePath);
        pageContentMap.put("folder-path", PathUtils.extractWebFolder(pagePath));
        pageContentMap.put("page-exists", wikiService.existsWikiFile(pagePath));
        pageContentMap.put("form-action-url", EscapeUtils.encodeUrl(EscapeUtils.pagePath2Url(formPath)));
        pageContentMap.put("from-pos", fromPos);
        pageContentMap.put("to-pos", toPos);
        pageContentMap.put("title-input-placeholder", messages.getMessage(EDITOR_INPUT_TITLE_KEY));
        pageContentMap.put("content-input-placeholder", messages.getMessage(EDITOR_INPUT_CONTENT_KEY));
        if (wikiService.existsWikiFile(pagePath)) {
            try {
                WikiText wikiText = wikiService.readWikiText(pagePath, fromPos, toPos);
                pageContentMap.put("content-input-text", wikiText.getText());
            } catch (ServiceException e) {
                logger.write("Wiki page '" + pagePath + "' not found", e);
                String msg = messages.getMessage(EDITOR_ERROR_KEY, pagePath, e.getMessage());
                throw new ServiceException(msg, e);
            }
        }
        pageContentMap.put("save-button-title", messages.getMessage(EDITOR_INPUT_SAVE_KEY));
        pageContentMap.put("cancel-button-title", messages.getMessage(EDITOR_INPUT_CANCEL_KEY));
        pageContentMap.put("delete-button-disabled", !wikiService.existsWikiFile(pagePath));
        pageContentMap.put("delete-button-title", messages.getMessage(EDITOR_INPUT_DELETE_KEY));
        pageContentMap.put("template-select-hint", messages.getMessage(EDITOR_INPUT_TEMPLATE_SELECT_KEY));
        pageContentMap.put("help-hint", messages.getMessage(EDITOR_HELP_KEY));
        pageContentMap.put("upload-hint", messages.getMessage(EDITOR_INPUT_UPLOAD_HINT_KEY));
        pageContentMap.put("upload-text", messages.getMessage(EDITOR_INPUT_UPLOAD_TITLE_KEY));
        pageContentMap.put("delete-confirmation-msg", messages.getMessage(DELETE_CONFIRMATION_KEY));

        // templates
        Map<String, String> templates = readTemplates();
        String[] templateNames = templates.keySet().stream().sorted(Comparator.naturalOrder()).toArray(String[]::new);
        pageContentMap.put("template-names", templateNames);
        for (String templateName : templates.keySet()) {
            String templateText = templates.get(templateName);
            pageContentMap.put("template-text--" + templateName, templateText);
        }

        // upload panel
        pageContentMap.put("uploadpanel-no-file-msg", messages.getMessage(UPLOAD_NO_FILE_SELECTED_KEY));
        pageContentMap.put("uploadpanel-multiple-files-msg", messages.getMessage(UPLOAD_MULTIPLE_FILES_SELECTED_KEY));
        pageContentMap.put("uploadpanel-too-big-msg", messages.getMessage(UPLOAD_FILE_TOO_BIG_KEY));
        pageContentMap.put("uploadpanel-header", messages.getMessage(EDITOR_UPLOADPANEL_TITLE_KEY));
        pageContentMap.put("uploadpanel-file-select-title", messages.getMessage(EDITOR_UPLOADPANEL_FILE_KEY));
        pageContentMap.put("uploadpanel-repository-path-label", messages.getMessage(EDITOR_UPLOADPANEL_REPOSITORY_PATH_KEY));
        pageContentMap.put("uploadpanel-image-tag-label", messages.getMessage(EDITOR_UPLOADPANEL_IMAGE_TAG_KEY));
        pageContentMap.put("uploadpanel-file-tag-label", messages.getMessage(EDITOR_UPLOADPANEL_FILE_TAG_KEY));
        pageContentMap.put("uploadpanel-save-button", messages.getMessage(EDITOR_UPLOADPANEL_SAVE_KEY));
        pageContentMap.put("uploadpanel-cancel-button", messages.getMessage(EDITOR_UPLOADPANEL_CANCEL_KEY));

        String contentJson = JavaScriptUtils.generateJsonMap(pageContentMap);
        byte[] contentBytes = contentJson.getBytes(StandardCharsets.UTF_8);
        return new HttpResponse(StatusCode.OK, ContentType.JSON_UTF8, contentBytes);
    }

    /**
     * Reads the wiki templates and sorts them by name.
     */
    @NotNull
    private Map<String, String> readTemplates() {
        String templatesPagePath = settings.getTemplatesPagePath();
        if (templatesPagePath == null) {
            return Collections.emptyMap();
        }

        try {
            Map<String, String> templates = new TreeMap<>();
            WikiFile templateParentPage = wikiService.getWikiFile(templatesPagePath);
            for (String pagePath : templateParentPage.getChildren()) {
                WikiFile templateWikiFile = wikiService.getWikiFile(pagePath);
                String templateContent = templateWikiFile.getWikiText();

                // remove first line with parent link to template page
                final String templateParent = "{{parent:" + templatesPagePath + "}}";
                if (templateContent.startsWith(templateParent)) {
                    templateContent = templateContent.substring(templateParent.length());
                    if (templateContent.startsWith("\r")) {
                        templateContent = templateContent.substring(1);
                    }
                    if (templateContent.startsWith("\n")) {
                        templateContent = templateContent.substring(1);
                    }
                }

                templates.put(pagePath, templateContent);
            }
            return templates;
        } catch (ServiceException e) {
            logger.write("Error reading template pages", e);
            return Collections.emptyMap();
        }
    }

    /**
     * Closes the editor, redirects to the page view.
     */
    @NotNull
    private HttpResponse cancelEditing(@NotNull String pagePath) {
       return htmlService.generateRedirectToWikiPage(pagePath);
    }

    /**
     * Deletes the wiki page and redirects to the start page.
     */
    @NotNull
    private HttpResponse deleteWikiPage(@NotNull String pagePath) throws ServiceException {
        try {
            wikiService.deleteWikiFile(pagePath);
            return htmlService.generateRedirectToWikiPage(settings.getStartpagePath());
        } catch (ServiceException e) {
            logger.write("Error deleting wiki page '" + pagePath + "'", e);
            String msg = messages.getMessage(DELETE_ERROR_KEY, e.getMessage());
            throw new ServiceException(msg, e);
        }
    }

    /**
     * Saves the editor content and redirects to the page view.
     *
     * @param oldPagePath Name of the wiki page to be edited.
     *                    null -> create new wiki page.
     * @param newPagePath New name of the wiki page in the page title bar.
     *                    If the user changes the wiki page name its file name is also renamed.
     *                    Must have at least one character.
     * @param newWikiText Wiki page content.
     *                    null -> empty (= "").
     * @param fromPos     null -> whole wiki page.
     * @param toPos       null -> whole wiki page.
     */
    @NotNull
    private HttpResponse saveWikiText(@Nullable String oldPagePath, @NotNull String newPagePath, @Nullable String newWikiText,
                                      @Nullable Integer fromPos, @Nullable Integer toPos) throws ServiceException {
        // catch invalid page name
        newPagePath = newPagePath.trim();
        if (newPagePath.length() == 0 || newPagePath.endsWith("/")) {
            logger.write("Cannot save wiki page with invalid name '" + newPagePath + "'");
            String msg = messages.getMessage(SAVE_INVALID_NAME_KEY, newPagePath);
            throw new ServiceException(msg);
        }
        if (newPagePath.charAt(0) != '/') {
            newPagePath = '/' + newPagePath;
        }

        // empty wiki content
        if (newWikiText == null) {
            newWikiText = "";
        }

        // catch renaming to an existing page
        if ((oldPagePath == null || !oldPagePath.equals(newPagePath)) && wikiService.existsWikiFile(newPagePath)) {
            logger.write("Cannot create wiki page '" + newPagePath + "' as there is already a page with the same name");
            String msg = messages.getMessage(SAVE_ALREADY_EXISTING_KEY, newPagePath);
            throw new ServiceException(msg);
        }

        try {
            // delete old wiki page if it is renamed
            if (oldPagePath != null && !oldPagePath.endsWith("/") && !oldPagePath.equals(newPagePath)) {
                wikiService.deleteWikiFile(oldPagePath);
            }

            // save wiki page
            WikiText wikiText = new WikiText(newWikiText, fromPos, toPos);
            wikiService.writeWikiText(newPagePath, wikiText);
        } catch (ServiceException e) {
            logger.write("Error saving wiki page '" + newPagePath + "'", e);
            String msg = messages.getMessage(SAVE_ERROR_KEY, newPagePath, e.getMessage());
            throw new ServiceException(msg, e);
        }

        // finally, show saved page
        String anchor = extractHeadingAnchor(newWikiText, fromPos, toPos);
        return htmlService.generateRedirectToWikiPage(newPagePath, anchor);
    }

    /**
     * If the wiki text starts with a heading (=== text), calculate the anchor name.
     *
     * @return Anchor name, null -> no heading found.
     */
    @Nullable
    static String extractHeadingAnchor(@NotNull String wikiText, @Nullable Integer fromPos, @Nullable Integer toPos) {
        if (fromPos == null || toPos == null || !wikiText.startsWith("=")) {
            // no heading found
            return null;
        }

        // extract heading text (ignores further wiki formatting)
        int endPos = wikiText.indexOf('\n');
        int crPos = wikiText.indexOf('\r');
        if (crPos >= 0 && crPos < endPos) {
            endPos = crPos;
        }
        if (endPos == -1) {
            endPos = wikiText.length();
        }
        String headingStr = wikiText.substring(0, endPos);
        while (headingStr.startsWith("=")) {
            headingStr = headingStr.substring(1);
        }
        while (headingStr.endsWith("=")) {
            headingStr = headingStr.substring(0, headingStr.length() - 1);
        }
        headingStr = headingStr.trim();

        // calculate anchor name
        if (headingStr.length() > 0) {
            return WikiHelper.getIdString(headingStr);
        } else {
            return null;
        }
    }

    /**
     * Shows the wiki editor.
     */
    @NotNull
    private HttpResponse showEditor(@NotNull String pagePath) throws ServiceException {
        String visiblePagePath = pagePath;
        if (!wikiService.existsWikiFile(pagePath)) {
            visiblePagePath = messages.getMessage(EDITOR_TITLE_NEW_PAGE_KEY);
        }
        WikiFile editorWikiFile = wikiService.getWikiFile("/wiki/Editor");
        WikiPage newEditorWikiPage = new WikiPage(visiblePagePath, editorWikiFile.getWikiPage(), 0, 0);
        WikiPage wikiPage = WikiHelper.extendWikiPage(newEditorWikiPage, true, false, false,
                logger, settings, wikiService);
        return htmlService.convertPage(wikiPage);
    }

    /**
	 * Uploads a binary file to the repository.
     */
    @NotNull
    public HttpResponse handleUploadRequest(@NotNull String urlPath, byte @NotNull [] httpBody) {
        // cut off "/upload" prefix
        String filePath = urlPath.substring(7).trim();

        // catch invalid filename
        if (filePath.length() == 0 || filePath.endsWith("/")) {
            logger.write("Upload file name '" + filePath + "' is invalid");
            String msg = messages.getMessage(UPLOAD_INVALID_NAME_KEY, filePath);
            return generateJsonResponse(StatusCode.CLIENT_BAD_REQUEST, msg);
        }
        // catch backwards path navigation
        if (filePath.contains("..")) {
            logger.write("Upload file name '" + filePath + "' contains illegal parent navigation");
            String msg = messages.getMessage(UPLOAD_PARENT_NAVIGATION_KEY, filePath);
            return generateJsonResponse(StatusCode.CLIENT_BAD_REQUEST, msg);
        }
        if (filePath.charAt(0) != '/') {
            filePath = '/' + filePath;
        }

        try {
            if (repositoryService.existsFile(filePath)) {
                logger.write("Upload file name '" + filePath + "' already exists");
                String msg = messages.getMessage(UPLOAD_ALREADY_EXISTING_KEY, filePath);
                throw new Exception(msg);
            }
            AnyFile anyFile = new AnyFile(filePath);
            repositoryService.writeBinaryFile(anyFile, httpBody, null);
            logger.write("File '" + filePath + "' successfully uploaded");
        } catch (Exception e) {
            logger.write("Error uploading file '" + filePath + "'", e);
            return generateJsonResponse(StatusCode.SERVER_INTERNAL_SERVER_ERROR, e.getMessage());
        }

        return generateJsonResponse(StatusCode.OK, "File upload successful: " + filePath);
    }

    private HttpResponse generateJsonResponse(@NotNull StatusCode statusCode, @NotNull String jsonText) {
        String contentStr = JavaScriptUtils.generateJsonMessage(jsonText);
        byte[] content = StringUtils.stringToUtf8Bytes(contentStr);
        return new HttpResponse(statusCode, ContentType.JSON_UTF8, content);
    }
}
