/*
 * MoasdaWiki Server
 *
 * Copyright (C) 2008 - 2023 Herbert Reiter (herbert@moasdawiki.net)
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License version 3 as
 * published by the Free Software Foundation (AGPL-3.0-only).
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

package net.moasdawiki.service.handler;

import net.moasdawiki.base.Logger;
import net.moasdawiki.base.ServiceException;
import net.moasdawiki.base.Settings;
import net.moasdawiki.http.StatusCode;
import net.moasdawiki.http.StatusCodeUtils;
import net.moasdawiki.service.HttpResponse;
import net.moasdawiki.service.render.HtmlService;
import net.moasdawiki.service.repository.AnyFile;
import net.moasdawiki.service.repository.RepositoryService;
import net.moasdawiki.util.PathUtils;
import org.jetbrains.annotations.NotNull;

/**
 * Handles download requests for static files.
 * The content type is derived from the file name suffix.
 */
public class FileDownloadHandler {

	private static final String FILE_NOT_FOUND_KEY = "FileDownloadHandler.fileNotFound";
	private static final String REVERSE_NAVIGATION_KEY = "FileDownloadHandler.reverseNavigation";

	private final Logger logger;
	private final Settings settings;
	private final RepositoryService repositoryService;
	private final HtmlService htmlService;

	/**
	 * Constructor.
	 */
	public FileDownloadHandler(@NotNull Logger logger, @NotNull Settings settings,
							   @NotNull RepositoryService repositoryService, @NotNull HtmlService htmlService) {
		this.logger = logger;
		this.settings = settings;
		this.repositoryService = repositoryService;
		this.htmlService = htmlService;
	}

	/**
	 * Handle file downloads for URL paths starting with "/img/".
	 */
	@NotNull
	public HttpResponse handleDownloadImg(@NotNull String urlPath) {
		String absolutePath = urlPath.substring(4); // cut off "/img"
		return generateFileResponse(absolutePath);
	}

	/**
	 * Handle file downloads for URL paths starting with "/file/".
	 */
	@NotNull
	public HttpResponse handleDownloadFile(@NotNull String urlPath) {
		String absolutePath = urlPath.substring(5); // cut off "/file"
		return generateFileResponse(absolutePath);
	}

	/**
	 * Handle file downloads for the URL paths starting with "/..." (no second "/").
	 *
	 * Example: /favicon.ico
	 */
	@NotNull
	public HttpResponse handleDownloadRoot(@NotNull String urlPath) {
		String rootPath = PathUtils.makeWebPathAbsolute(null, settings.getRootPath());
		String absolutePath = PathUtils.concatWebPaths(rootPath, urlPath);
		return generateFileResponse(absolutePath);
	}

	/**
	 * Reads the given file from the repository and generates a HTTP response object.
	 * Returns an error message if the file doesn't exist.
	 */
	@NotNull
	private HttpResponse generateFileResponse(@NotNull String filePath) {
		if (filePath.contains("..")) {
			logger.write("File path '" + filePath + "' contains invalid reverse navigation, sending response 403 access denied.");
			return htmlService.generateErrorPage(StatusCode.CLIENT_FORBIDDEN, REVERSE_NAVIGATION_KEY, filePath);
		}

		byte[] fileContent;
		try {
			AnyFile anyFile = new AnyFile(filePath);
			fileContent = repositoryService.readBinaryFile(anyFile);
		} catch (ServiceException e) {
			logger.write("File '" + filePath + "' not found, sending response 404", e);
			return htmlService.generateErrorPage(StatusCode.CLIENT_NOT_FOUND, FILE_NOT_FOUND_KEY, filePath);
		}

		return new HttpResponse(StatusCode.OK, StatusCodeUtils.contentTypeByFileSuffix(filePath), fileContent);
	}
}
