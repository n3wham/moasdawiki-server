/*
 * MoasdaWiki Server
 *
 * Copyright (C) 2008 - 2023 Herbert Reiter (herbert@moasdawiki.net)
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License version 3 as
 * published by the Free Software Foundation (AGPL-3.0-only).
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

package net.moasdawiki.service.handler;

import net.moasdawiki.base.Logger;
import net.moasdawiki.base.ServiceException;
import net.moasdawiki.base.Settings;
import net.moasdawiki.http.StatusCode;
import net.moasdawiki.service.HttpResponse;
import net.moasdawiki.service.render.HtmlService;
import net.moasdawiki.service.wiki.WikiFile;
import net.moasdawiki.service.wiki.WikiHelper;
import net.moasdawiki.service.wiki.WikiService;
import net.moasdawiki.service.wiki.structure.WikiPage;
import net.moasdawiki.util.EscapeUtils;
import org.jetbrains.annotations.NotNull;

/**
 * Renders a wiki page as a HTML page and adds the navigation area, header
 * section and footer section.
 *
 * If the URL path ends with "/" it shows the folder index page. By default,
 * the page "IndexStandard" is used as template. This can be overridden by a
 * page "Index" in the folder.
 */
public class ViewPageHandler {

	private static final String INDEX_DISABLED_KEY = "ViewPageHandler.index.disabled";
	private static final String INDEX_NOT_FOUND_KEY = "ViewPageHandler.index.notfound";
	private static final String PAGE_NOT_FOUND_KEY = "ViewPageHandler.page.notfound";

	private final Logger logger;
	private final Settings settings;
	private final WikiService wikiService;
	private final HtmlService htmlService;

	/**
	 * Constructor.
	 */
	public ViewPageHandler(@NotNull Logger logger, @NotNull Settings settings,
						   @NotNull WikiService wikiService, @NotNull HtmlService htmlService) {
		this.logger = logger;
		this.settings = settings;
		this.wikiService = wikiService;
		this.htmlService = htmlService;
	}

	/**
	 * Is called for URL path "/".
	 */
	@NotNull
	public HttpResponse handleRootPath() {
		String filePath = settings.getStartpagePath();
		return showWikiPage(filePath);
	}

	/**
	 * Is called for URL path starting with "/view/".
	 */
	@NotNull
	public HttpResponse handleViewPath(@NotNull String urlPath) {
		String filePath = urlPath.substring(5);
		filePath = EscapeUtils.url2PagePath(filePath);
		if (filePath.endsWith("/")) {
			return showFolderIndex(filePath);
		} else {
			return showWikiPage(filePath);
		}
	}

	/**
	 * Render the folder index page.
	 */
	@NotNull
	private HttpResponse showFolderIndex(@NotNull String folderPath) {
		String indexName = settings.getIndexPageName();
		String indexDefaultPagePath = settings.getIndexFallbackPagePath();
		if (indexName == null || indexDefaultPagePath == null) {
			return htmlService.generateErrorPage(StatusCode.CLIENT_FORBIDDEN, INDEX_DISABLED_KEY);
		}

		// Index page in folder available?
		if (wikiService.existsWikiFile(folderPath + indexName)) {
			// Index page available, use it to render the page
			return showWikiPage(folderPath + indexName);
		}

		// Global index template page available?
		try {
			WikiFile indexWikiFile = wikiService.getWikiFile(indexDefaultPagePath);
			WikiPage wikiPage = new WikiPage(folderPath + indexName, indexWikiFile.getWikiPage(), null, null);
			wikiPage = WikiHelper.extendWikiPage(wikiPage, true, true, true, logger, settings, wikiService);
			return htmlService.convertPage(wikiPage);
		}
		catch (ServiceException e) {
			logger.write("Error reading default index page, sending 404", e);
			return htmlService.generateErrorPage(StatusCode.CLIENT_NOT_FOUND, INDEX_NOT_FOUND_KEY, folderPath);
		}
	}

	/**
	 * Render a wiki page.
	 * An error message is shown if the page doesn't exist.
	 */
	@NotNull
	private HttpResponse showWikiPage(@NotNull String filePath) {
		try {
			WikiFile wikiFile = wikiService.getWikiFile(filePath);
			WikiPage wikiPage = WikiHelper.extendWikiPage(wikiFile.getWikiPage(), true, true, true,
					logger, settings, wikiService);
			if (wikiPage.getPagePath() != null) {
				wikiService.addLastViewedWikiFile(wikiPage.getPagePath());
			}
			return htmlService.convertPage(wikiPage);
		}
		catch (ServiceException e) {
			logger.write("Error reading wiki page, sending 404", e);
			return htmlService.generateErrorPage(StatusCode.CLIENT_NOT_FOUND, PAGE_NOT_FOUND_KEY, filePath);
		}
	}
}
