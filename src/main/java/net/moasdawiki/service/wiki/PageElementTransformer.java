/*
 * MoasdaWiki Server
 *
 * Copyright (C) 2008 - 2023 Herbert Reiter (herbert@moasdawiki.net)
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License version 3 as
 * published by the Free Software Foundation (AGPL-3.0-only).
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

package net.moasdawiki.service.wiki;

import net.moasdawiki.service.wiki.structure.PageElement;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Signature of a lambda function that transforms a PageElement to another PageElement.
 */
@FunctionalInterface
public interface PageElementTransformer {

	/**
	 * Transforms a PageElement to another PageElement.
	 *
	 * This method is called for all elements in a PageElement nodes in a
	 * WikiPage, except for PageElementList types.
	 *
	 * If the method returns null, the element is removed from the list.
	 *
	 * The parameter <tt>pageElement</tt> may be modified.
	 * 
	 * @param pageElement Element in a WikiPage.
	 * @return Transformed element that replaces the old element.
	 * 		   null -> remove the old element.
	 */
	@Nullable
	PageElement transformPageElement(@NotNull PageElement pageElement);
}
