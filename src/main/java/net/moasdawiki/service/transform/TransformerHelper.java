/*
 * MoasdaWiki Server
 *
 * Copyright (C) 2008 - 2023 Herbert Reiter (herbert@moasdawiki.net)
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License version 3 as
 * published by the Free Software Foundation (AGPL-3.0-only).
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

package net.moasdawiki.service.transform;

import net.moasdawiki.service.wiki.PageElementTransformer;
import net.moasdawiki.service.wiki.structure.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Helper methods for transforming wiki pages.
 */
public class TransformerHelper {

    /**
     * Traverses the whole wiki tree of a WikiPage.
     *
     * Calls the transformer method for each node in the wiki tree and replaces
     * it by the result value.
     *
     * @param wikiPage WikiPage to transform.
     * @param transformer Transformer method to call for each node in the wiki tree.
     * @return Result WikiPage.
     */
    @NotNull
    public static WikiPage transformPageElements(@NotNull WikiPage wikiPage, @NotNull PageElementTransformer transformer) {
        PageElement pe = transformPageElement(wikiPage, transformer);

        if (pe instanceof WikiPage) {
            return (WikiPage) pe;
        } else {
            return new WikiPage(wikiPage.getPagePath(), pe, wikiPage.getFromPos(), wikiPage.getToPos());
        }
    }

    /**
     * Processes the given PageElement sub-tree.
     */
    @Nullable
    private static PageElement transformPageElement(@Nullable PageElement pageElement, @NotNull PageElementTransformer transformer) {
        if (pageElement == null) {
            return null;
        }
        if (!(pageElement instanceof PageElementList)) {
            pageElement = transformer.transformPageElement(pageElement);
        }

        // process hierarchical elements
        if (pageElement instanceof PageElementWithChild) {
            return transformPageElementWithChild(((PageElementWithChild) pageElement), transformer);
        } else if (pageElement instanceof PageElementList) {
            return transformPageElementList((PageElementList) pageElement, transformer);
        } else if (pageElement instanceof Table) {
            return transformTable((Table) pageElement, transformer);
        } else {
            // null / no child elements / unknown type
            return pageElement;
        }
    }

    /**
     * Processes a PageElementList node.
     */
    @NotNull
    private static PageElementList transformPageElementList(@NotNull PageElementList pageElementList, @NotNull PageElementTransformer transformer) {
        int i = 0;
        while (i < pageElementList.size()) {
            // Listeneintrag transformieren
            PageElement pe = pageElementList.get(i);
            pe = transformPageElement(pe, transformer);

            // geänderten Eintrag in Liste austauschen bzw. löschen
            if (pe != null) {
                pageElementList.set(i, pe);
                i++;
            } else {
                pageElementList.remove(i);
            }
        }
        return pageElementList;
    }

    /**
     * Processes a PageElementWithChild node.
     */
    @NotNull
    private static PageElement transformPageElementWithChild(@NotNull PageElementWithChild pageElement, @NotNull PageElementTransformer transformer) {
        PageElement child = pageElement.getChild();
        child = transformPageElement(child, transformer);
        pageElement.setChild(child);
        return pageElement;
    }

    /**
     * Processes a Table node.
     */
    @NotNull
    private static Table transformTable(@NotNull Table table, @NotNull PageElementTransformer transformer) {
        for (TableRow tableRow : table.getRows()) {
            for (TableCell tableCell : tableRow.getCells()) {
                PageElement pe = transformPageElement(tableCell.getContent(), transformer);
                tableCell.setContent(pe);
            }
        }
        return table;
    }
}
