/*
 * MoasdaWiki Server
 *
 * Copyright (C) 2008 - 2023 Herbert Reiter (herbert@moasdawiki.net)
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License version 3 as
 * published by the Free Software Foundation (AGPL-3.0-only).
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

package net.moasdawiki.server;

import net.moasdawiki.http.HttpRequest;
import net.moasdawiki.http.StatusCode;
import net.moasdawiki.service.HttpResponse;
import net.moasdawiki.service.handler.EditorHandler;
import net.moasdawiki.service.handler.FileDownloadHandler;
import net.moasdawiki.service.handler.SearchHandler;
import net.moasdawiki.service.handler.ViewPageHandler;
import net.moasdawiki.service.render.HtmlService;
import net.moasdawiki.service.sync.SynchronizationService;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Forwards incoming HTTP requests to the corresponding service method.
 */
public class RequestDispatcher {

    private final HtmlService htmlService;
    private final ViewPageHandler viewPageHandler;
    private final SearchHandler searchHandler;
    private final EditorHandler editorHandler;
    private final FileDownloadHandler fileDownloadHandler;

    /**
     * Is null in App.
     */
    @Nullable
    private final SynchronizationService synchronizationService;

    /**
     * Constructor.
     */
    public RequestDispatcher(@NotNull HtmlService htmlService, @NotNull ViewPageHandler viewPageHandler,
                             @NotNull SearchHandler searchHandler, @Nullable EditorHandler editorHandler,
                             @NotNull FileDownloadHandler fileDownloadHandler,
                             @Nullable SynchronizationService synchronizationService) {
        this.htmlService = htmlService;
        this.viewPageHandler = viewPageHandler;
        this.searchHandler = searchHandler;
        this.editorHandler = editorHandler;
        this.fileDownloadHandler = fileDownloadHandler;
        this.synchronizationService = synchronizationService;
    }

    /**
     * This method is called on every incoming HTTP request. It forwards the
     * call to a corresponding service method. The response is transformed to
     * a HTTP response.
     */
    @NotNull
    public HttpResponse handleRequest(@NotNull HttpRequest httpRequest) {
        String urlPath = httpRequest.getUrlPath();
        HttpResponse httpResponse = null;
        if (urlPath.equals("/")) {
            httpResponse = viewPageHandler.handleRootPath();
        }
        else if (urlPath.startsWith("/view/")) {
            httpResponse = viewPageHandler.handleViewPath(urlPath);
        }
        else if (urlPath.startsWith("/search/")) {
            String query = httpRequest.getParameter("text");
            httpResponse = searchHandler.handleSearchRequest(query);
        }
        else if (urlPath.startsWith("/edit/") && editorHandler != null) {
            httpResponse = editorHandler.handleEditRequest(httpRequest);
        }
        else if (urlPath.startsWith("/upload") && editorHandler != null) {
            httpResponse = editorHandler.handleUploadRequest(urlPath, httpRequest.getHttpBody());
        }
        else if (urlPath.startsWith("/sync")) {
            if (synchronizationService != null) {
                httpResponse = synchronizationService.handleSyncRequest(httpRequest);
            }
        }
        else if (urlPath.startsWith("/img/")) {
            httpResponse = fileDownloadHandler.handleDownloadImg(urlPath);
        }
        else if (urlPath.startsWith("/file/")) {
            httpResponse = fileDownloadHandler.handleDownloadFile(urlPath);
        }
        else if (urlPath.startsWith("/") && urlPath.lastIndexOf('/') == 0) {
            httpResponse = fileDownloadHandler.handleDownloadRoot(urlPath);
        }

        if (httpResponse == null) {
            httpResponse = htmlService.generateErrorPage(StatusCode.CLIENT_NOT_FOUND, "wiki.server.url.unmapped", httpRequest.getUrlPath());
        }
        return httpResponse;
    }
}
