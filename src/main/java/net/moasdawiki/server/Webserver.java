/*
 * MoasdaWiki Server
 *
 * Copyright (C) 2008 - 2023 Herbert Reiter (herbert@moasdawiki.net)
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License version 3 as
 * published by the Free Software Foundation (AGPL-3.0-only).
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

package net.moasdawiki.server;

import net.moasdawiki.base.Logger;
import net.moasdawiki.base.ServiceException;
import net.moasdawiki.base.Settings;
import net.moasdawiki.http.HttpRequest;
import net.moasdawiki.http.StatusCode;
import net.moasdawiki.service.HttpResponse;
import net.moasdawiki.service.render.HtmlService;
import net.moasdawiki.util.EscapeUtils;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Runs a simple webserver.
 * <p>
 * Although the client requests are handles single-threaded, incoming requests
 * are accepted by a thread pool. This is necessary as e.g. the Firefox web
 * browser typically opens several parallel request connections, some of them
 * are "speculative" to speed up subsequent requests which would block the
 * incoming queue until the request times out. For more details see:
 * https://bugzilla.mozilla.org/show_bug.cgi?id=853423
 * https://bugs.chromium.org/p/chromium/issues/detail?id=85229
 */
public class Webserver {

    /**
     * Timeout for reading the incoming request HTTP header.
     * Firefox uses 6 seconds timeout for its "speculative requests",
     * thus should be more.
     */
    private static final int REQUEST_READ_TIMEOUT = 10000; // 10 seconds
    private static final String CRLF = "\r\n";

    @NotNull
    private final Logger log;

    @NotNull
    private final Settings settings;

    @NotNull
    private final HtmlService htmlService;

    @NotNull
    private final RequestDispatcher requestDispatcher;

    /**
     * Is user allowed to shutdown the server?
     * Should not be allowed in case of a daemon service.
     * Default value: <code>false</code>
     */
    private boolean shutdownRequestAllowed;

    /**
     * Indicates if the server is ready to accept incoming requests.
     */
    private boolean running;

    private ServerSocket server; // initialized by run()
    private ExecutorService threadPool; // initialized by run()
    private final Object synchronizationLock = new Object();

    /**
     * Constructor.
     */
    public Webserver(@NotNull Logger log, @NotNull Settings settings,
                     @NotNull HtmlService htmlService, @NotNull RequestDispatcher requestDispatcher) {
        super();
        this.log = log;
        this.settings = settings;
        this.htmlService = htmlService;
        this.requestDispatcher = requestDispatcher;
    }

    public void setShutdownRequestAllowed(boolean shutdownRequestAllowed) {
        this.shutdownRequestAllowed = shutdownRequestAllowed;
    }

    /**
     * Starts the server.
     * Blocks until the server is stopped by {@link #stop()} or by o user request.
     */
    public void run() {
        int port = settings.getServerPort();
        threadPool = Executors.newFixedThreadPool(5);
        try (ServerSocket server = new ServerSocket(port)) {
            this.server = server;
            log.write("Wiki server listening on port " + server.getLocalPort());

            running = true;
            //noinspection InfiniteLoopStatement
            while (true) {
                // will throw an Exception after server.close() is called
                final Socket client = server.accept();
                threadPool.execute(() -> handleConnection(client));
            }
        } catch (IOException e) {
            log.write("Error running server on port " + settings.getServerPort(), e);
        }
        running = false;
        threadPool.shutdown();
        log.write("Server stopped");
    }

    /**
     * Stops the server.
     * Must be called to properly free resources on shutdown.
     */
    public void stop() {
        log.write("Server received stop signal");

        // cancel blocked accept() calls
        try {
            if (server != null) {
                server.close();
            }
        } catch (IOException e) {
            log.write("Error closing server", e);
        }

        if (threadPool != null) {
            threadPool.shutdown();
        }
    }

    public boolean isRunning() {
        return running;
    }

    private void handleConnection(Socket client) {
        long ts1 = System.currentTimeMillis();
        try {
            HttpResponse response = null;
            HttpRequest httpRequest;
            try {
                // important, otherwise a request could block forever
                client.setSoTimeout(REQUEST_READ_TIMEOUT);

                httpRequest = HttpRequestParser.parse(client.getInputStream());
                log.write("Incoming request from " + client.getRemoteSocketAddress() + ": " + httpRequest.getMethod() + " " + httpRequest.getUrlPath());
            } catch (Exception e) {
                // don't log if connection is closed (speculative requests)
                if (!client.isClosed()) {
                    long ts2 = System.currentTimeMillis();
                    log.write("Error handling a request after " + (ts2 - ts1) + " ms", e);
                }
                // Don't send an HTTP response if no HTTP headers were received (e.g. for speculative requests)
                // otherwise Chromium shows the error response after the next request.
                return;
            }

            try {
                synchronized (synchronizationLock) {
                    // ab hier nur noch single-threaded
                    response = generateResponse(httpRequest, client);
                }
            } catch (Exception e) {
                log.write("Error generating response", e);
            }

            try {
                // send response to client
                if (response != null && !client.isClosed() && !client.isOutputShutdown()) {
                    writeResponse(response, client.getOutputStream());
                }
            } catch (Exception e) {
                log.write("Error sending the response of a request", e);
            }
        } finally {
            try {
                client.close();
            } catch (IOException e) {
                log.write("Error closing socket connection", e);
            }
        }
    }

    /**
     * Handle a request and generate a response.
     */
    private HttpResponse generateResponse(HttpRequest httpRequest, Socket client) {
        // restrict access to localhost
        if (settings.isOnlyLocalhostAccess() && !client.getInetAddress().isLoopbackAddress()) {
            log.write("Remote access from " + client.getInetAddress().getHostAddress() + " denied");
            return htmlService.generateErrorPage(StatusCode.CLIENT_FORBIDDEN, "wiki.server.onlylocalhost");
        }

        // shutdown command
        if ("/shutdown".equals(httpRequest.getUrlPath())) {
            if (shutdownRequestAllowed) {
                try {
                    server.close();
                } catch (IOException e) {
                    log.write("Error closing server on user request", e);
                }
                return htmlService.generateMessagePage("wiki.server.shutdown.finished");
            } else {
                return htmlService.generateErrorPage(StatusCode.CLIENT_FORBIDDEN, "wiki.server.shutdown.denied");
            }
        }

        // dispatch request to corresponding service
        return requestDispatcher.handleRequest(httpRequest);
    }

    /**
     * Write HTTP response to connection output stream corresponding to RFC 2616.
     */
    private void writeResponse(@NotNull HttpResponse httpResponse, @NotNull OutputStream out) throws ServiceException {
        try {
            StringBuilder header = new StringBuilder();
            header.append("HTTP/1.1 ");
            header.append(httpResponse.getStatusCode().getCode());
            header.append(' ');
            header.append(httpResponse.getStatusCode().getMessage());
            header.append(CRLF);

            header.append("Server: MoasdaWiki");
            header.append(CRLF);

            if (httpResponse.getContentType() != null) {
                header.append("Content-Type: ");
                header.append(httpResponse.getContentType().getMediaType());
                header.append(CRLF);
            }

            if (httpResponse.getRedirectUrl() != null) {
                header.append("Location: ");
                header.append(EscapeUtils.encodeUrl(httpResponse.getRedirectUrl()));
                header.append(CRLF);
            }

            header.append("Content-Length: ");
            int contentLength = httpResponse.getContent() != null ? httpResponse.getContent().length : 0;
            header.append(contentLength);
            header.append(CRLF);

            header.append("Cache-Control: ");
            if (httpResponse.getContentType() != null && httpResponse.getContentType().isStaticContent()) {
                header.append("max-age=86400"); // 86400 seconds = 24 hours
            } else {
                header.append("no-cache");
            }
            header.append(CRLF);

            header.append("X-Content-Type-Options: nosniff");
            header.append(CRLF);
            header.append("Content-Security-Policy: default-src 'none'; script-src 'self'; connect-src 'self'; img-src 'self'; style-src 'self'; base-uri 'self'; form-action 'self';");
            header.append(CRLF);
            header.append("Cross-Origin-Opener-Policy: same-origin");
            header.append(CRLF);
            header.append("Cross-Origin-Resource-Policy: same-origin");
            header.append(CRLF);

            header.append("Connection: close");
            header.append(CRLF);

            // end of HTTP header
            header.append(CRLF);

            // write HTTP header
            //noinspection CharsetObjectCanBeUsed
            byte[] headerData = header.toString().getBytes("UTF-8");
            out.write(headerData);

            // write HTTP body
            if (httpResponse.getContent() != null) {
                out.write(httpResponse.getContent());
            }
            out.flush();
        } catch (UnsupportedEncodingException e) {
            throw new ServiceException("Error converting HTTP header into UTF-8", e);
        } catch (IOException e) {
            throw new ServiceException("Error writing HTTP response stream", e);
        }
    }
}
