/*
 * MoasdaWiki Server
 *
 * Copyright (C) 2008 - 2023 Herbert Reiter (herbert@moasdawiki.net)
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License version 3 as
 * published by the Free Software Foundation (AGPL-3.0-only).
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

package net.moasdawiki.server;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import net.moasdawiki.base.ServiceException;
import net.moasdawiki.http.ContentType;
import net.moasdawiki.http.HttpRequest;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Parses a HTTP request according to RFC 2616 (HTTP/1.1).
 * Supports UTF-8 character encoding only.
 *
 * Beispiel:
 * 
 * <pre>
 * GET /index.html HTTP/1.1
 * Host: www.moasdawiki.net
 * </pre>
 * 
 * This class is thread-safe.
 */
public abstract class HttpRequestParser {

	/**
	 * Parse the HTTP request.
	 * 
	 * @param inputStream Input stream to read the request data from.
	 * @return HTTP request object.
	 */
	@NotNull
	public static HttpRequest parse(@NotNull InputStream inputStream) throws ServiceException {
		Map<String, String> httpHeader = readHttpHeader(inputStream);

		// Parse first HTTP header line
		String requestLine = httpHeader.get(HttpRequest.HTTP_HEADER_FIRST_LINE);
		String method = extractMethod(requestLine);
		String url = extractUrl(requestLine);
		String urlPath = extractUrlPath(url);
		Map<String, String> urlParameters = extractUrlParameters(url);

		// Read HTTP body
		String contentLengthStr = httpHeader.get(HttpRequest.HTTP_HEADER_CONTENT_LENGTH);
		byte[] httpBody = readHttpBody(contentLengthStr, inputStream);

		// Read POST form data if available
		String contentType = httpHeader.get(HttpRequest.HTTP_HEADER_CONTENT_TYPE);
		if (HttpRequest.METHOD_POST.equals(method) && ContentType.FORM_DATA.getMediaType().equals(contentType)) {
			Map<String, String> bodyParameters = extractPostData(httpBody);
			if (urlParameters.isEmpty()) {
				urlParameters = bodyParameters;
			} else {
				urlParameters.putAll(bodyParameters);
			}
		}

		return new HttpRequest(httpHeader, method, url, urlPath, urlParameters, httpBody);
	}

	/**
	 * Parse the HTTP header lines.
	 */
	private static Map<String, String> readHttpHeader(InputStream is) throws ServiceException {
		Map<String, String> result = new HashMap<>();
		try {
			while (true) {
				String line = readLine(is);
				if (line.isEmpty()) {
					// Empty line marks end of HTTP header
					break;
				} else if (result.isEmpty()) {
					// First HTTP header line
					result.put(HttpRequest.HTTP_HEADER_FIRST_LINE, line);
				} else {
					// Other HTTP header lines have the format "Key: Value"
					int pos = line.indexOf(':');
					if (pos > 0) {
						// case-insensitive --> convert in lowercase
						String name = line.substring(0, pos).trim().toLowerCase();
						String value = line.substring(pos + 1).trim();
						result.put(name, value);
					} else {
						// Ignore invalid formatted lines
						System.out.println("Invalid HTTP header: " + line);
					}
				}
			}
			if (result.isEmpty()) {
				throw new ServiceException("Empty HTTP header");
			}
			return result;
		} catch (Exception e) {
			throw new ServiceException("Error reading HTTP headers", e);
		}
	}

	/**
	 * Reads a single line from the input stream.
	 * According to RFC 2616 \n and \r\n are accepted as line delimiter.
	 * Doesn't detect end of stream, so only call method for header lines.
	 */
	@NotNull
	private static String readLine(@NotNull InputStream is) throws IOException {
		StringBuilder line = new StringBuilder();
		int b;
		while ((b = is.read()) >= 0) {
			//noinspection StatementWithEmptyBody
			if (b == '\r') {
				// Zeichen ignorieren
			} else if (b == '\n') {
				break; // Zeilenende erreicht
			} else {
				// Zeichen hinzufügen
				line.append((char) b);
			}
		}
		return line.toString();
	}

	/**
	 * Extracts the HTTP request method.
	 * Example: <tt>GET</tt>
	 */
	@NotNull
	private static String extractMethod(@NotNull String requestLine) throws ServiceException {
		int pos = requestLine.indexOf(' ');
		if (pos > 0) {
			return requestLine.substring(0, pos);
		} else {
			throw new ServiceException("Invalid first HTTP header row: " + requestLine);
		}
	}

	/**
	 * Extracts the URL of the HTTP request.
	 * Example: <tt>/path/action?param1=value1&amp;param2=value2</tt>
	 */
	@NotNull
	private static String extractUrl(@NotNull String requestLine) throws ServiceException {
		int pos1 = requestLine.indexOf(' ');
		int pos2 = requestLine.indexOf(' ', pos1 + 1);
		if (pos1 < 0 || pos2 < 0) {
			throw new ServiceException("Invalid first HTTP header row: " + requestLine);
		}
		String encodedUrl = requestLine.substring(pos1 + 1, pos2);

		try {
			// URL dekodieren, bei %xy-Angaben UTF-8 berücksichtigen
			return URLDecoder.decode(encodedUrl, StandardCharsets.UTF_8);
		} catch (Exception e) {
			throw new ServiceException("Invalid request URL: " + encodedUrl, e);
		}
	}

	/**
	 * Extracts the URL path without "?" and the parameters.
	 * Example: <tt>/path/action</tt>
	 */
	@NotNull
	private static String extractUrlPath(@NotNull String url) throws ServiceException {
		int pos = url.indexOf('?');
		String result;
		if (pos < 0) {
			// URL hat keine Parameter
			result = url;
		} else {
			// Parameter abschneiden
			result = url.substring(0, pos);
		}
		if (result.isEmpty()) {
			throw new ServiceException("Invalid request URL, path is empty: " + url);
		}
		return result;
	}

	/**
	 * Extracts the URL parameters.
	 */
	@NotNull
	private static Map<String, String> extractUrlParameters(@NotNull String url) {
		int pos = url.indexOf('?');
		if (pos < 0) {
			// keine Parameter vorhanden
			return Collections.emptyMap();
		}
		return parseUrlParameters(url.substring(pos + 1));
	}

	/**
	 * Parses the URL parameters into key value pairs.
	 *
	 * Example:
	 * Parameter string: <tt>param1=value1&amp;param2=value2</tt><br>
	 * Key value pairs: <tt>param1=value1</tt>, <tt>param2=value2</tt>
	 */
	@NotNull
	private static Map<String, String> parseUrlParameters(@NotNull String queryStr) {
		Map<String, String> result = new HashMap<>();
		int index = 0;
		while (index < queryStr.length()) {
			String entry;
			int separatorpos = queryStr.indexOf("&", index);
			if (separatorpos >= index) {
				entry = queryStr.substring(index, separatorpos);
				index = separatorpos + 1;
			} else {
				entry = queryStr.substring(index);
				index = queryStr.length(); // Ende der Post-Daten erreicht
			}

			int middlepos = entry.indexOf("=");
			if (middlepos >= 0) {
				String name = URLDecoder.decode(entry.substring(0, middlepos), StandardCharsets.UTF_8);
				String value = URLDecoder.decode(entry.substring(middlepos + 1), StandardCharsets.UTF_8);
				result.put(name, value);
			}
		}
		return result;
	}

	/**
	 * Reads the HTTP body data from the input stream.
	 * The HTTP header must already be read.
	 */
	private static byte @NotNull [] readHttpBody(@Nullable String contentLengthStr, @NotNull InputStream is) throws ServiceException {
		if (contentLengthStr == null) {
			// no body available, e.g. in case of a GET request
			return new byte[0];
		}

		int contentLength;
		try {
			contentLength = Integer.parseInt(contentLengthStr);
		} catch (Exception e) {
			throw new ServiceException("Invalid Content-Length: " + contentLengthStr, e);
		}

		try {
			byte[] httpBody = new byte[contentLength];
			int count = 0;
			int read;
			while (count < contentLength && (read = is.read(httpBody, count, contentLength - count)) >= 0) {
				count += read;
			}
			return httpBody;
		} catch (IOException e) {
			throw new ServiceException("Error reading HTTP body data", e);
		}
	}

	/**
	 * Extracts POST form data from the HTTP body.
	 */
	@NotNull
	private static Map<String, String> extractPostData(byte @NotNull [] httpBody) throws ServiceException {
		try {
			//noinspection CharsetObjectCanBeUsed
			String postData = new String(httpBody, "UTF-8");
			return parseUrlParameters(postData);
		} catch (UnsupportedEncodingException e) {
			throw new ServiceException("Error reading HTTP POST data", e);
		}
	}
}
