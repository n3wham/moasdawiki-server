/*
 * MoasdaWiki Server
 *
 * Copyright (C) 2008 - 2023 Herbert Reiter (herbert@moasdawiki.net)
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License version 3 as
 * published by the Free Software Foundation (AGPL-3.0-only).
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

package net.moasdawiki.http;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Map;

/**
 * Contains the parsed data of an HTTP request.
 */
public class HttpRequest {

    public static final String HTTP_HEADER_FIRST_LINE = "";
    public static final String HTTP_HEADER_CONTENT_TYPE = "content-type";
    public static final String HTTP_HEADER_CONTENT_LENGTH = "content-length";
    public static final String METHOD_GET = "GET";
    public static final String METHOD_POST = "POST";

    /**
     * HTTP headers.
     * The first header line is mapped to the key "".
     * As the key names are case-insensitive according to RFC 2616 they
     * are converted to lower-case.
     */
    @NotNull
    final private Map<String, String> httpHeader;

    /**
     * HTTP request method.
     * Examples: <tt>GET</tt>, <tt>POST</tt>
     */
    @NotNull
    final private String method;

    /**
     * Full URL incl. path and parameters.
     * Example: <tt>/pfad/action?param1=value1&amp;param2=value2</tt>
     */
    @NotNull
    final private String url;

    /**
     * URL path, without "?" and parameters.
     * Example: <tt>/pfad/action</tt>
     */
    @NotNull
    final private String urlPath;

    /**
     * URL parameters or HTML form field values.
     * Parameters in the HTTP body have higher priority than URL parameters.
     */
    @NotNull
    final private Map<String, String> parameters;

    /**
     * HTTP body raw data.
     */
    final private byte @NotNull [] httpBody;

    /**
     * Constructor.
     */
    public HttpRequest(@NotNull Map<String, String> httpHeader, @NotNull String method,
                @NotNull String url, @NotNull String urlPath,
                @NotNull Map<String, String> parameters, byte @NotNull [] httpBody) {
        this.httpHeader = httpHeader;
        this.method = method;
        this.url = url;
        this.urlPath = urlPath;
        this.parameters = parameters;
        this.httpBody = httpBody;
    }

    @Nullable
    public String getHttpHeader(@NotNull String key) {
        return httpHeader.get(key);
    }

    @NotNull
    public String getMethod() {
        return method;
    }

    @NotNull
    public String getUrl() {
        return url;
    }

    @NotNull
    public String getUrlPath() {
        return urlPath;
    }

    @Nullable
    public String getParameter(String key) {
        return parameters.get(key);
    }

    public byte @NotNull [] getHttpBody() {
        return httpBody;
    }
}
