/*
 * MoasdaWiki Server
 *
 * Copyright (C) 2008 - 2023 Herbert Reiter (herbert@moasdawiki.net)
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License version 3 as
 * published by the Free Software Foundation (AGPL-3.0-only).
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

package net.moasdawiki.http;

import org.jetbrains.annotations.NotNull;

/**
 * Represents an HTTP content type used in HTTP requests and responses.
 */
public enum ContentType {
    BINARY("application/octet-stream", false),
    CSS("text/css", true),
    FORM_DATA("application/x-www-form-urlencoded", false),
    HTML("text/html", false),
    IMAGE_GIF("image/gif", true),
    IMAGE_JPEG("image/jpeg", true),
    IMAGE_PNG("image/png", true),
    IMAGE_SVG("image/svg+xml", true),
    IMAGE_ICON("image/x-icon", true),
    JAVASCRIPT("application/javascript", true),
    JSON_UTF8("application/json; charset=utf-8", false),
    PDF("application/pdf", true),
    PLAIN("text/plain", false),
    XML("text/xml", false),
    ZIP("application/zip", true);

    /**
     * Media type of the content. Formerly known as MIME type.
     *
     * See https://www.iana.org/assignments/media-types/media-types.xhtml
     */
    @NotNull
    private final String mediaType;

    /**
     * Is the content static? This is used for the cache control setting.
     */
    private final boolean staticContent;

    ContentType(@NotNull String mediaType, boolean staticContent) {
        this.mediaType = mediaType;
        this.staticContent = staticContent;
    }

    @NotNull
    public String getMediaType() {
        return mediaType;
    }

    public boolean isStaticContent() {
        return staticContent;
    }
}
