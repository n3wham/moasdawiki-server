/*
 * MoasdaWiki Server
 *
 * Copyright (C) 2008 - 2023 Herbert Reiter (herbert@moasdawiki.net)
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License version 3 as
 * published by the Free Software Foundation (AGPL-3.0-only).
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see
 * <https://www.gnu.org/licenses/agpl-3.0.html>.
 */

package net.moasdawiki.http;

/**
 * Represents an HTTP response status code.
 */
@SuppressWarnings("unused")
public enum StatusCode {
    OK(200, "OK"),
    REDIRECTION_MOVED_PERMANENTLY(301, "Moved Permanently"),
    REDIRECTION_MOVED_TEMPORARILY(302, "Moved Temporarily"),
    CLIENT_BAD_REQUEST(400, "Bad Request"),
    CLIENT_FORBIDDEN(403, "Forbidden"),
    CLIENT_NOT_FOUND(404, "Not Found"),
    SERVER_INTERNAL_SERVER_ERROR(500, "Internal Server Error"),
    SERVER_NOT_IMPLEMENTED(501, "Not Implemented");

    /**
     * Status code number.
     */
    private final int code;

    /**
     * Status code message.
     */
    private final String message;

    StatusCode(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
