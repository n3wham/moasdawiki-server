{{html}}
<div class="wiki-editor" id="wiki-editor">
    <form method="post" action="" enctype="application/x-www-form-urlencoded" name="EditForm">
        <input type="hidden" name="fromPos" value="" />
        <input type="hidden" name="toPos" value="" />
        <input type="text" class="titleinput" name="titleeditor" value="" placeholder="" />
        <textarea class="textinput" cols="80" rows="10" name="contenteditor" placeholder="">
</textarea>
        <div class="controlarea" id="controlarea">
            <div class="section">
                <button type="submit" name="save" class="save"></button>
                <button type="submit" name="cancel" class="cancel"></button>
            </div>
            <div class="section">
                <button type="button" name="deleteButton" class="delete"></button>
            </div>
            <div class="section">
                <select name="TemplateSelect" class="TemplateSelect"></select>
                <a href="/view/wiki/syntax/" target="_blank" id="help-hint"></a>
            </div>
            <div class="uploadarea" id="uploadarea" title=""></div>
        </div>
    </form>
</div>

<div class="uploadpanel" id="uploadpanel-root">
    <div class="panelbackground"></div>
    <div class="panel">
        <div class="header" id="uploadpanel-header"></div>
        <div class="body">
            <form method="post" enctype="application/x-www-form-urlencoded" name="uploadForm">
                <div class="section" id="uploadpanel-file-select">
                    <label for="uploadpanel-file-select-input" id="uploadpanel-file-select-label"></label><br>
                    <input type="file" id="uploadpanel-file-select-input" name="fileSelect" />
                </div>
                <div class="section">
                    <label for="uploadpanel-repository-path-input" id="uploadpanel-repository-path-label"></label><br>
                    <input type="text" id="uploadpanel-repository-path-input" name="uploadRepositoryPath" />
                </div>
                <div class="section">
                    <input type="checkbox" id="uploadpanel-image-tag-input" name="generateImageTag" checked="true">
                    <label for="uploadpanel-image-tag-input" id="uploadpanel-image-tag-label"></label><br>
                    <input type="checkbox" id="uploadpanel-file-tag-input" name="generateFileTag" checked="true">
                    <label for="uploadpanel-file-tag-input" id="uploadpanel-file-tag-label"></label>
                </div>
                <div class="footer">
                    <button type="button" name="uploadButton" class="save" id="uploadpanel-save-button"></button>
                    <button type="button" name="cancelButton" class="cancel" id="uploadpanel-cancel-button"></button>
                </div>
            </form>
        </div>
    </div>
</div>
{{/html}}