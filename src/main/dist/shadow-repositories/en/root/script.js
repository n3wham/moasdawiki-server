/*
MoasdaWiki Server

Copyright (C) 2008 - 2023 Herbert Reiter (herbert@moasdawiki.net)

This program is free software: you can redistribute it and/or modify it
under the terms of the GNU Affero General Public License version 3 as
published by the Free Software Foundation (AGPL-3.0-only).

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see
<https://www.gnu.org/licenses/agpl-3.0.html>.
*/

//******************************************
//  Wiki editor
//******************************************

let editorContent;
let lastSelectedFile;

// Wait until HTML page is completely loaded
window.addEventListener('load', function() {
    if (document.getElementById("wiki-editor")) {
        initWikiEditor();
    }
});

// Initialize wiki editor.
async function initWikiEditor() {
    console.log("Initialize wiki editor");
    editorContent = await loadEditorContent();
    fillPageFields();

    // set cursor in input field
	if (editorContent["page-exists"]) {
		const field = document.forms["EditForm"].elements["contenteditor"];
		field.focus();
		field.selectionStart = 0;
		field.selectionEnd = 0;
		field.scrollTop = 0;
	} else {
		const field = document.forms["EditForm"].elements["titleeditor"];
		field.focus();
		field.selectionStart = field.value.length;
		field.selectionEnd = field.value.length;
	}

	// attach listeners
	document.forms["EditForm"].elements["deleteButton"].addEventListener("click", deleteButtonClicked, false);
	document.forms["EditForm"].elements["TemplateSelect"].addEventListener("change", templateSelectChanged, false);
	const uploadarea = document.getElementById('uploadarea');
	uploadarea.addEventListener("click", uploadareaClicked, false);
	uploadarea.addEventListener("dragover", handleFileDragOver, false);
	uploadarea.addEventListener("dragleave", handleFileDragOver, false);
	uploadarea.addEventListener("drop", handleFileDrop, false);
	const uploadPanelfileInput = document.getElementById('uploadpanel-file-select-input');
	uploadPanelfileInput.addEventListener('change', handleFileSelect, false);
	const uploadPanelSaveButton = document.getElementById('uploadpanel-save-button');
	uploadPanelSaveButton.addEventListener('click', uploadpanelSaveClicked, false);
	const uploadPanelCancelButton = document.getElementById('uploadpanel-cancel-button');
	uploadPanelCancelButton.addEventListener('click', uploadpanelCancelClicked, false);
};

async function loadEditorContent() {
    let url = location.pathname + "?json=true";
    const urlParams = new URLSearchParams(window.location.search);
    const fromPos = urlParams.get('fromPos');
    const toPos = urlParams.get('toPos');
    if (fromPos && toPos) {
        url += "&fromPos=" + encodeURIComponent(fromPos) + "&toPos=" + encodeURIComponent(toPos);
    }
    console.log("Load editor content: " + url);
    try {
        const response = await fetch(url);
        if (response.ok) {
            const jsonObj = await response.json();
            return jsonObj;
        }
    } catch(err) {
        console.log("Error loading editor content: " + err);
    }
}

function fillPageFields() {
    console.log("Editor content object:");
    console.log(editorContent);

    // editor area
    document.forms["EditForm"].action = editorContent["form-action-url"];
    if (editorContent["from-pos"]) {
        document.forms["EditForm"].elements["fromPos"].value = editorContent["from-pos"];
    }
    if (editorContent["to-pos"]) {
        document.forms["EditForm"].elements["toPos"].value = editorContent["to-pos"];
    }
    document.forms["EditForm"].elements["titleeditor"].value = editorContent["page-path"];
    document.forms["EditForm"].elements["titleeditor"].placeholder = editorContent["title-input-placeholder"];
    document.forms["EditForm"].elements["contenteditor"].placeholder = editorContent["content-input-placeholder"];
    if (editorContent["content-input-text"]) {
        document.forms["EditForm"].elements["contenteditor"].value = editorContent["content-input-text"];
    }
    document.forms["EditForm"].elements["save"].appendChild(document.createTextNode(editorContent["save-button-title"]));
    document.forms["EditForm"].elements["cancel"].appendChild(document.createTextNode(editorContent["cancel-button-title"]));
    document.forms["EditForm"].elements["deleteButton"].appendChild(document.createTextNode(editorContent["delete-button-title"]));
    if (editorContent["delete-button-disabled"]) {
        document.forms["EditForm"].elements["deleteButton"].disabled = true;
    }
    const selectOption = document.createElement('option');
    selectOption.appendChild(document.createTextNode(editorContent["template-select-hint"]));
    document.forms["EditForm"].elements["TemplateSelect"].appendChild(selectOption);
    document.getElementById("help-hint").appendChild(document.createTextNode(editorContent["help-hint"]));
    document.getElementById("uploadarea").title = editorContent["upload-hint"];
    document.getElementById("uploadarea").appendChild(document.createTextNode(editorContent["upload-text"]));

    // template dropdown
    const templateNames = editorContent["template-names"];
    console.log(templateNames);
    const templateSelectTag = document.forms["EditForm"].elements["TemplateSelect"];
    for (i = 0; i < templateNames.length; i++) {
        templateSelectTag.options[templateSelectTag.options.length] = new Option(templateNames[i]);
    }

    // upload panel
    document.getElementById("uploadpanel-header").appendChild(document.createTextNode(editorContent["uploadpanel-header"]));
    document.getElementById("uploadpanel-file-select-label").appendChild(document.createTextNode(editorContent["uploadpanel-file-select-title"]));
    document.getElementById("uploadpanel-repository-path-label").appendChild(document.createTextNode(editorContent["uploadpanel-repository-path-label"]));
    document.getElementById("uploadpanel-image-tag-label").appendChild(document.createTextNode(editorContent["uploadpanel-image-tag-label"]));
    document.getElementById("uploadpanel-file-tag-label").appendChild(document.createTextNode(editorContent["uploadpanel-file-tag-label"]));
    document.getElementById("uploadpanel-save-button").appendChild(document.createTextNode(editorContent["uploadpanel-save-button"]));
    document.getElementById("uploadpanel-cancel-button").appendChild(document.createTextNode(editorContent["uploadpanel-cancel-button"]));
}

function deleteButtonClicked() {
	if (confirm(editorContent["delete-confirmation-msg"])) {
		const form = document.forms['EditForm'];
		const el = document.createElement('input');
		el.type = 'hidden';
		el.name = 'delete';
		el.value = 'delete';
		form.appendChild(el);
		form.submit();
	}
};

function templateSelectChanged(event) {
    const selectedIndex = event.target.selectedIndex;
    console.log("Selected index: " + selectedIndex);
	if (selectedIndex >= 1) {
	    const templateName = event.target.value;
		const templateContent = editorContent["template-text--" + templateName];
		document.forms["EditForm"].elements["contenteditor"].value += templateContent;
	}
};

function uploadareaClicked(event) {
    showPanel();
}

// Visual effect of "uploadares" during drag&drop.
function handleFileDragOver(event) {
	event.stopPropagation();
	event.preventDefault();
	if (event.type == 'dragover') {
		addCssClass('uploadarea', 'dragover');
	} else {
		removeCssClass('uploadarea', 'dragover');
	}
};

// Called after a file is dropped on the upload area.
function handleFileDrop(event) {
	// remove hover
	handleFileDragOver(event);

	const files = event.dataTransfer.files;
	if (!files) {
		return;
	}
	if (files.length < 1) {
		alert(editorContent["uploadpanel-no-file-msg"]);
		return;
	}
	if (files.length > 1) {
		alert(editorContent["uploadpanel-multiple-files-msg"]);
		return;
	}
	if (files[0].size > 10000000) { // 10 MB
		alert(editorContent["uploadpanel-too-big-msg"]);
		return;
	}

	showPanel(files[0]);
};

// Called if the user selects a file in the panel.
function handleFileSelect(event) {
	let files = event.target.files;
	if (!files) {
		// nichts tun
	} else if (files.length < 1) {
		alert(editorContent["uploadpanel-no-file-msg"]);
		files = null;
	} else if (files.length > 1) {
		alert(editorContent["uploadpanel-multiple-files-msg"]);
		files = null;
	} else if (files[0].size > 10000000) { // 10 MB
		alert(editorContent["uploadpanel-too-big-msg"]);
		files = null;
	}

	if (files && files.length > 0) {
		fillUploadInfo(files[0]);
	} else {
		fillUploadInfo();
	}
};

// Initialize the upload panel and make it visible.
// selectedFile: File, dropped on upload area, null -> show file select in panel
function showPanel(selectedFile) {
	document.forms["uploadForm"].reset();

	const showFileSelect = (typeof (selectedFile) == 'undefined');
	setDisplay('uploadpanel-file-select', showFileSelect);

	fillUploadInfo(selectedFile);

	setDisplay("uploadpanel-root", true);
};

// Hide the upload panel.
function hidePanel() {
	setDisplay("uploadpanel-root", false);
};

function fillUploadInfo(selectedFile) {
	const selectedFileDefined = (typeof selectedFile != 'undefined');

	// Suggest a repository file name
	let uploadPath = editorContent["folder-path"];
	if (selectedFileDefined) {
		uploadPath += selectedFile.name;
	}
	document.forms["uploadForm"].elements["uploadRepositoryPath"].value = uploadPath;
	document.forms["uploadForm"].elements["uploadRepositoryPath"].disabled = !selectedFileDefined;

	// show image tag checkbox only for an image
	const isImage = (selectedFile && selectedFile.name.match(/\.(gif|jpg|jpeg|png)$/i) != null);
	document.forms["uploadForm"].elements["generateImageTag"].checked = isImage;
	document.forms["uploadForm"].elements["generateImageTag"].disabled = !isImage;
	setCssClass('uploadpanel-image-tag-label', 'disabled', !document.forms["uploadForm"].elements["generateImageTag"].checked);

	// download link
	document.forms["uploadForm"].elements["generateFileTag"].disabled = !selectedFileDefined;
	document.forms["uploadForm"].elements["generateFileTag"].checked = selectedFileDefined
			&& !document.forms["uploadForm"].elements["generateFileTag"].checked;
	setCssClass('uploadpanel-file-tag-label', 'disabled', !selectedFileDefined);

	// upload button
	document.forms["uploadForm"].elements["uploadButton"].disabled = !selectedFileDefined;

	// remember file
	lastSelectedFile = selectedFile;
};

function uploadpanelSaveClicked(event) {
    handleFileUpload();
}

function uploadpanelCancelClicked(event) {
    hidePanel();
}

// Called if user clicks on upload button.
function handleFileUpload() {
	let uploadPath = document.forms["uploadForm"].elements["uploadRepositoryPath"].value;
	if (uploadPath.length > 0 && uploadPath.charAt(0) != '/') {
		// make relative path absolute
		uploadPath = editorContent["folder-path"] + uploadPath;
	}

	const url = '/upload' + uploadPath;
    console.log("Uploading file: " + url);
	fetch(url, {
            method: 'POST',
            body: lastSelectedFile
        })
        .then(response => response.json().then(res => ({ json: res, status: response.status })))
        .then(result => {
            if (result.status === 200) {
                console.log("File upload successful");
                console.log(result);
                // add tags to wiki page content
                if (document.forms["uploadForm"].elements["generateImageTag"].checked) {
                    var tag = '{{image:' + getShortPath(uploadPath) + '}}\n';
                    insertIntoTextarea(document.forms["EditForm"].elements["contenteditor"], tag);
                }
                if (document.forms["uploadForm"].elements["generateFileTag"].checked) {
                    var tag = '[[file:' + getShortPath(uploadPath) + ']]\n';
                    insertIntoTextarea(document.forms["EditForm"].elements["contenteditor"], tag);
                }
                hidePanel();
            } else {
                console.log("File upload failed!");
                console.log(result);
                alert(result.json.message);
            }
        })
        .catch(error => {
            console.log("File upload failed!");
            console.log(error);
            alert(error);
        });
};

function getShortPath(path) {
	if (path.indexOf(editorContent["folder-path"]) == 0) {
		// same folder or subfolder --> relative
		return path.substring(editorContent["folder-path"].length);
	} else {
		// different folder --> keep absolute
		return path;
	}
};

/******************************************
  JavaScript support for synchronization
*******************************************/

if (document.getElementById("sync-control")) {
    initSynchronizationControl();
}

function initSynchronizationControl() {
    console.log("Initializing synchronization control");
    const syncDiv = document.getElementById("sync-control");
    const buttons = syncDiv.querySelectorAll("button");
    buttons.forEach(button => button.addEventListener("click", syncButtonClicked, false));
    console.log(buttons.length + " buttons found");
}

function syncButtonClicked(event) {
    const sessionId = event.target.dataset.sessionId;
    const action = event.target.dataset.action;
    console.log("Button clicked: " + action + " session '" + sessionId + "'");
    if (action === "permit") {
        syncSessionPermit(sessionId);
    }
    else if (action === "drop") {
        syncSessionDrop(sessionId);
    }
}

function syncSessionPermit(sessionId) {
	var url = '/sync-gui/session-permit?session-id=' + sessionId;
	fetch(url)
	    .then(response => response.json())
	    .then(json => {
	        console.log(json);
	        if (json.code === 0) {
	            console.log("Session successfully permitted");
	            location.reload();
	        } else {
	            alert("Error permitting session");
	        }
	    })
	    .catch(error => {
	        console.log(error);
            alert("Error permitting session");
	    });
};

function syncSessionDrop(sessionId) {
	var url = '/sync-gui/session-drop?session-id=' + sessionId;
	fetch(url)
	    .then(response => response.json())
	    .then(json => {
	        console.log(json);
	        if (json.code === 0) {
	            console.log("Session successfully dropped");
	            location.reload();
	        } else {
	            alert("Error dropping session");
	        }
	    })
	    .catch(error => {
	        console.log(error);
            alert("Error dropping session");
	    });
};

/******************************************
  General helper methods
*******************************************/

// Add a CSS class to a HTML element.
function addCssClass(elementOrId, cssClass) {
	var elementNode;
	if (typeof (elementOrId) == 'string') {
		elementNode = document.getElementById(elementOrId);
	} else {
		elementNode = elementOrId;
	}
	if (elementNode && !isCssClassSet(elementOrId, cssClass)) {
		elementNode.className += ' ' + cssClass;
	}
};

// Remove a CSS class from a HTML element.
function removeCssClass(elementOrId, cssClass) {
	var elementNode;
	if (typeof (elementOrId) == 'string') {
		elementNode = document.getElementById(elementOrId);
	} else {
		elementNode = elementOrId;
	}
	if (elementNode) {
		var re = new RegExp('(?:^|\\s)' + cssClass + '(?!\\S)', 'g');
		elementNode.className = elementNode.className.replace(re, '');
	}
};

// Add or remove a CSS class to/from a HTML element.
function setCssClass(elementOrId, cssClass, enable) {
	if (enable) {
		addCssClass(elementOrId, cssClass);
	} else {
		removeCssClass(elementOrId, cssClass);
	}
};

// Check if a CSS class is present at a HTML element.
function isCssClassSet(elementOrId, cssClass) {
	var elementNode;
	if (typeof (elementOrId) == 'string') {
		elementNode = document.getElementById(elementOrId);
	} else {
		elementNode = elementOrId;
	}
	var re = new RegExp('(?:^|\\s)' + cssClass + '(?!\\S)', '');
	return (elementNode && (elementNode.className.match(re) != null));
};

// Makes a HTML element visible or invisible.
// show: boolean
function setDisplay(elementOrId, show) {
	var elementNode;
	if (typeof (elementOrId) == 'string') {
		elementNode = document.getElementById(elementOrId);
	} else {
		elementNode = elementOrId;
	}

	if (show && show == true) {
		elementNode.style.display = 'block';
	} else {
		elementNode.style.display = 'none';
	}
};

// Inserts text in the text area.
function insertIntoTextarea(elementOrId, text) {
	var elementNode;
	if (typeof (elementOrId) == 'string') {
		elementNode = document.getElementById(elementOrId);
	} else {
		elementNode = elementOrId;
	}
	if (typeof document.selection != 'undefined') {
		// Internet Explorer
		var range = document.selection.createRange();
		range.text = text;
	} else if (typeof elementNode.selectionStart != 'undefined') {
		// Gecko based browsers
		var start = elementNode.selectionStart;
		var end = elementNode.selectionEnd;
		elementNode.value = elementNode.value.substr(0, start) + text + elementNode.value.substr(end);
		elementNode.selectionStart = start + text.length;
		elementNode.selectionEnd = start + text.length;
	}
};
